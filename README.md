# Scalable Gaussian processes in TensorFlow

Documentation is a work in progress, but functional examples are available in
[`examples/`](examples).

## Functionality

Gaussian process regression library based on 1) Krylov subspace linear algebra
(as opposed to dense Cholesky decompositions and similar), and 2) kernel
interpolation techniques to allow exploitation of Kronecker and Toeplitz
structure.

In particular, the following numerical techniques are employed:

1. Linear solves with conjugate gradient.
2. Log determinants with
   1. [stochastic trace estimators & Lanczos quadrature](https://scholar.google.com/scholar?cluster=14056534852336449544&hl=en&as_sdt=0,5&scilib=1) and
   2. scaled eigenvalues.
3. SKI/KISS-GP: Linear [kernel interpolation](https://scholar.google.com/scholar?cluster=10688700732600694368&hl=en&as_sdt=0,5&scilib=1) (cubic planned).
4. Toeplitz matrix-vector multiplies implemented with FFT convolutions.

Python 3 only. (E.g. uses type annotations extensively.)

## Quickstart

Install (requires TensorFlow, NumPy, and `attrs`) (not available on PyPI yet...).

```sh
pip install git+ssh://git@gitlab.com/joshburkart/gpscale.git
```

Get some independent- and dependent-variable data ready (`X` and `Y` respectively).

```python
import gpscale as gps
import tensorflow as tf

# `_in` suffix to indicate input data.
X_in = <...>
Y_in = <...>
# `_out` suffix to indicate outputs, i.e. interpolation locations.
X_out = <...>
```

Define a kernel, and optionally enable kernel interpolation (see below).

```python
# RBF kernel with trainable length scale with initial value of
# 1 (the default).
kernel = gps.kernels.Gaussian()
# Trainable overall variance scale with an initial value of
# 0.04. (Pass in a constant to make not trainable, or pass in a
# `tf.Variable` to control variable creation yourself.)
kernel *= gps.kernels.Scalar(gps.hyper.positive(init_value=0.04))
# Enable kernel interpolation. (Multi-dimensional also
# available.)
kernel = kernel.make_grid_interp_kernel(
    gps.Grid1D(<X_min>, <X_max>, <num_points>))
```

Compute posterior mean and log likelihood by building a `Model`.

```python
model = gps.Model.build(
    X_in=X_in,
    Y_in=Y_in,
    X_out=X_out,
    kernel=kernel,
)

# Posterior mean.
f_out_tens = model.f_out_mean

# Log likelihood.
log_likelihood = model.losses.log_likelihood
```

We can then use `log_likelihood` for hyperparameter training, using typical
TensorFlow optimizers like `tf.train.AdamOptimizer`. To actually run
computations, use a TensorFlow `Session`, or explore TF's eager execution mode.

## Literature references

Based on techniques developed in the following papers (among others!):

* Wilson, Andrew, and Hannes Nickisch. "Kernel interpolation for scalable structured Gaussian processes (KISS-GP)." International Conference on Machine Learning. 2015.
* Pleiss, Geoff, et al. "Constant-Time Predictive Distributions for Gaussian Processes." arXiv preprint arXiv:1803.06058 (2018).
* Dong, Kun, et al. "Scalable log determinants for gaussian process kernel learning." Advances in Neural Information Processing Systems (2017): 6327-6337.
* Gardner, Jacob, et al. "Gpytorch: Blackbox matrix-matrix gaussian process inference with gpu acceleration." Advances in Neural Information Processing Systems (2018): 7586-7596.
* Ubaru, Shashanka, Jie Chen, and Yousef Saad. "Fast Estimation of tr(f(A)) via Stochastic Lanczos Quadrature." SIAM Journal on Matrix Analysis and Applications 38.4 (2017): 1075-1099.

See also a recent review of scalable Gaussian process techniques/codes:

* Liu, Haitao, et al. "When Gaussian Process Meets Big Data: A Review of Scalable GPs." arXiv preprint arXiv:1807.01065 (2018).

## Links

See also [GPyTorch](https://gpytorch.readthedocs.io/en/latest/), which is built off PyTorch instead of TensorFlow.

## FAQ

* Is the code compatible with TF's eager execution mode?
  * Haven't tested yet... I think it's unlikely to require substantial changes, though.
