import setuptools

with open('requirements.txt', 'r') as f:
    install_requires = f.readlines()

with open('requirements_test.txt', 'r') as f:
    test_install_requires = f.readlines()

setuptools.setup(
    name="gpscale",
    version="0.0.1",
    author="Josh Burkart",
    description="Scalable Gaussian processes",
    packages=setuptools.find_packages(),
    extras_require={
        'test': test_install_requires,
    },
    install_requires=install_requires,
)
