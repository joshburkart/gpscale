"""1D "sausage plot" example.

To run:

```python
PYTHONPATH=. python3 examples/simple_1d.py
```
"""

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import gpscale as gps
from examples import util


def main():
    X_in = np.array([23., 29., 87.])
    Y_in = np.array([10., 10., 1.])

    run(X_in, Y_in, 10)
    run(X_in, Y_in, 20)
    run(X_in, Y_in, 100)
    run(X_in, Y_in, 1000)

    plt.scatter(X_in, Y_in, s=15, facecolors='gray', edgecolors='black')
    plt.legend()

    plt.show()


def run(X_in, Y_in, num_grid_points):
    with tf.Graph().as_default():  # pylint: disable=not-context-manager
        linspace_args = (0., 100., num_grid_points)
        grid = gps.Grid1D(*linspace_args)
        kernel = (
            gps.kernels.Scalar(10.**2) *
            gps.kernels.RBF(5.).make_grid_interp_kernel(grid)  #
        )

        X_out = np.linspace(*linspace_args)
        X_out = 0.1 + X_out[:-1]
        f_out = gps.Model.build(
            X_in=X_in,
            X_out=X_out,
            Y_in=Y_in,
            kernel=kernel,
            noise_kernel=gps.kernels.WhiteNoise(10.),
            mean_func=gps.ConstantMean(4.),
            solve_params=dict(verbose=True, enforce_tol=False, max_iter=1000),
        ).f_out_mean

        with util.make_session() as session:
            session.run(tf.global_variables_initializer())
            f_out = session.run(f_out)

    plt.plot(X_out, f_out, label=f'N = {num_grid_points}', alpha=0.85)


if __name__ == '__main__':
    main()
