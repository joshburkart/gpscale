"""1D hyperparameter learning example.

To run:

```python
PYTHONPATH=. python3 examples/sinc.py
```
"""

# pylint: disable=too-many-locals,too-many-statements

import time

import attr
import click
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import gpscale as gps
from gpscale import kernels as k
from examples import util

X_MIN = -15.
X_MAX = 15.

NOISE_LEVEL = 0.01


@click.command()
@click.option(
    '--kernel',
    type=click.Choice(['gaussian', 'periodic', 'spectral']),
    default='gaussian')
@click.option('--grid_points', type=int, default=400)
@click.option('--data_points', type=int, default=500)
@click.option('--train_steps', type=int, default=100)
@click.option('--profile/--no-profile', default=False)
@click.option('--train/--no-train', default=True)
@click.option('--xla/--no-xla', default=False)
@click.option('--plot/--no-plot', default=True)
@click.option('--tensorboard/--no-tensorboard', default=False)
@util.Args.decorate
def main(args):
    np.random.seed(1)

    X_in = np.random.uniform(low=X_MIN, high=X_MAX, size=args.data_points)
    X_in = X_in[(X_in < -4.5) | (X_in > 4.5)]
    f_in = _func(X_in)
    Y_in = f_in + NOISE_LEVEL * np.random.normal(size=len(X_in))

    with tf.Graph().as_default():  # pylint: disable=not-context-manager
        bounds = (X_MIN, X_MAX)

        if args.kernel == 'gaussian':
            base_kernel = k.Scalar() * k.Gaussian()
        elif args.kernel == 'periodic':
            base_kernel = k.Scalar() * k.Periodic() * k.Gaussian()
        elif args.kernel == 'spectral':
            base_kernel = k.SpectralMixture.make(
                num_components=5,
                init_min_length_scale=0.3,
                init_max_length_scale=X_MAX - X_MIN)
        else:
            raise ValueError(args.kernel)
        kernel = base_kernel.make_grid_interp_kernel(
            gps.Grid1D(*bounds, args.grid_points))

        X_in_ph = tf.placeholder(
            dtype=tf.float64, shape=tf.TensorShape([None]))
        Y_in_ph = tf.placeholder(
            dtype=tf.float64, shape=tf.TensorShape([None]))
        X_out = np.linspace(*bounds, 300)

        print('Building model...')
        model = gps.Model.build(
            X_in=X_in_ph,
            Y_in=Y_in_ph,
            X_out=X_out,
            kernel=kernel,
            solve_params=dict(
                tol=1e-2, max_iter=1000, jitter=1e-4, enforce_tol=True),
            det_params=dict(
                method=gps.DetMethod.SCALED_EVAL,
                solve_params=dict(
                    jitter=1e-2, max_iter=100, enforce_tol=False),
            ),
        )
        util.add_trainable_summaries()
        util.add_loss_summaries(model.losses)

        loss_comps = model.losses
        f_out_tens = model.f_out_mean

        with tf.variable_scope('Train'):
            opt = tf.train.AdamOptimizer(learning_rate=0.05)

            print('Building train op...')
            if args.train:
                train_op = opt.minimize(-loss_comps.log_likelihood)
            else:
                train_op = tf.no_op()

        if args.plot:
            plot_manager = _PlotManager()

            def plot(*args, **kwargs):
                plot_manager.plot(*args, **kwargs)
        else:

            def plot(*args, **kwargs):
                del args
                del kwargs

        with util.make_session(profile=args.profile, tensorboard=args.profile
                               or args.tensorboard, xla=args.xla) as session:
            feed_dict = {X_in_ph: X_in, Y_in_ph: Y_in}

            session.run(tf.global_variables_initializer(), feed_dict=feed_dict)
            print('Variables:')
            util.print_variables(session)

            run = session.run_with_profile if args.profile else session.run

            print('Computing initial loss...')
            losses, f_out = run(
                [loss_comps, f_out_tens], summaries=True, feed_dict=feed_dict)
            print(f'Initial losses: {_format_losses(losses)}')
            plot(X_in, Y_in, X_out=X_out, f_out=f_out)

            print('Training...')
            if not args.train:
                print('  (but not really)')

            start_time_ns = time.time_ns()
            for i in range(args.train_steps):
                losses, f_out, _ = run(
                    [loss_comps, f_out_tens, train_op],
                    summaries=True,
                    feed_dict=feed_dict)
                plot(X_in, Y_in, X_out=X_out, f_out=f_out)
                print(f"Step {i}: {_format_losses(losses)}")

                print('Variables:')
                util.print_variables(session)
            end_time_ns = time.time_ns()
            training_time_s = (end_time_ns - start_time_ns) / 1e9
            print(f'Training time: {training_time_s:.3g} s')


def _format_losses(losses):
    return (
        f"LogLikelihood={losses.log_likelihood:.3g}, "
        f"DataFit={losses.data_fit:.3g}, "
        f"Complexity={losses.complexity:.3g}")


@attr.dataclass(frozen=False)
class _PlotManager:
    already_drew: bool = False
    index: int = 0

    def plot(self, X_in, Y_in, X_out=None, f_out=None):
        if self.already_drew:
            plt.cla()

        x = np.linspace(X_MIN, X_MAX, 300)
        f = _func(x)
        plt.plot(x, f, label='True')
        if not self.already_drew:
            plt.ion()
            self.already_drew = True

        plt.scatter(
            X_in,
            Y_in,
            alpha=0.7,
            linewidths=0.5,
            edgecolors='black',
            s=10,
            c='green',
            label='Measurements')
        plt.title(f'Training step {str(self.index)}')
        self.index += 1
        if f_out is not None:
            plt.plot(X_out, f_out, label='Interp')
        plt.legend(loc='upper right')
        plt.tight_layout()
        plt.draw()
        plt.pause(0.001)


# def _func(x):
#     return 12.2 * np.sinc(2. * np.pi * x / 6.3) + 18.
def _func(x):
    return np.sinc(x) + np.sinc(x + 10) + np.sinc(x - 10)


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
