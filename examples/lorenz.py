"""Learning a kernel to interpolate/forecast for the Lorenz system.

To run:

```python
PYTHONPATH=. python3 examples/lorenz.py
```
"""

import time
import typing

import attr
import click
import numpy as np
import scipy.integrate
import tensorflow as tf
from matplotlib import pyplot as plt
from matplotlib import animation

import gpscale as gps
from examples import util

# Don't train on data prior to `_T_MIN` since we use a random initial condition,
# necessitating some "burn-in" time.
_T_MIN = 10
_T_MAX_TRAIN = _T_MIN + 60
_T_MAX_TEST = _T_MAX_TRAIN + 5
_PLOT_POINTS = 3000
_T_GRID = np.linspace(_T_MIN, _T_MAX_TEST, _PLOT_POINTS)

# Use this variable from the 3D Lorenz system for training.
_TRAINING_VAR_INDEX = 1

_DTYPE = tf.float32


@click.command()
@click.option('--grid_points', type=int, default=_PLOT_POINTS)
@click.option('--data_points', type=int, default=900)
@click.option('--train_steps', type=int, default=150)
@click.option('--profile/--no-profile', default=False)
@click.option('--tensorboard/--no-tensorboard', default=False)
@util.Args.decorate
def main(args):  # pylint: disable=too-many-locals
    np.random.seed(1)

    with tf.Graph().as_default():  # pylint: disable=not-context-manager
        linspace_args = (0, _T_MAX_TEST, args.grid_points)
        grid = gps.Grid1D(*linspace_args)

        # base_kernel = gps.kernels.Scalar(
        #     gps.hyper.positive(init_value=0.04)) * gps.kernels.Gaussian()
        base_kernel = gps.kernels.SpectralMixture.make(
            num_components=3,
            init_min_length_scale=0.5,
            init_max_length_scale=10,
            init_variance_scale=3**2)
        kernel = base_kernel.make_grid_interp_kernel(grid)

        print('Building model...')
        model = gps.Model.build(
            X_in=tf.placeholder(dtype=_DTYPE, shape=[args.data_points, 1]),
            Y_in=tf.placeholder(dtype=_DTYPE, shape=[args.data_points]),
            X_out=tf.placeholder(dtype=_DTYPE, shape=[args.grid_points, 1]),
            kernel=kernel,
            mean_func=gps.ConstantMean(),
            solve_params=dict(max_iter=100, enforce_tol=False),
            det_params=dict(solve_params=dict(max_iter=1000)),
        )
        loss_comps = model.losses

        util.add_trainable_summaries()
        util.add_loss_summaries(loss_comps)

        with tf.variable_scope('Train'):
            opt = tf.train.AdamOptimizer(learning_rate=0.01)

            print('Building train op...')
            train_op = opt.minimize(-loss_comps.log_likelihood)

        with util.make_session(
                profile=args.profile,
                tensorboard=args.profile or args.tensorboard,
        ) as session:
            session.run(tf.global_variables_initializer())
            print('Variables:')
            util.print_variables(session)

            trainer = _Trainer(
                model=model,
                train_op=train_op,
                session=session,
                data_points=args.data_points)

            print('Training...')

            fig, ax = plt.subplots()
            fig.set_size_inches(10, 4)
            ax.set(
                xlim=[_T_MIN, _T_MAX_TEST],
                ylim=[-20, 20],
                title='Interpolation/forecasting for the Lorenz system')
            f_true_line, = ax.plot([], [], dashes=[2, 1])
            Y_in_points = ax.scatter(
                [], [], facecolors='none', edgecolors='magenta', s=8)
            f_out_line, = ax.plot([], [])
            legend = ax.legend(
                [
                    f_true_line,
                    Y_in_points,
                    f_out_line,
                ],
                [
                    'Lorenz',
                    'Synth. measurements',
                    'GP interp./forecast',
                ],
            ),

            fig.tight_layout()

            def animate(i):
                sol, X_in, Y_in, f_out = trainer.step(i)
                f_true = sol(_T_GRID)[_TRAINING_VAR_INDEX]

                f_true_line.set_data(_T_GRID, f_true)
                Y_in_points.set_offsets(np.stack([X_in, Y_in], axis=1))
                f_out_line.set_data(_T_GRID, f_out)

                return (f_true_line, Y_in_points, f_out_line, *legend)

            def init():
                f_true_line.set_data([], [])
                Y_in_points.set_offsets([])
                f_out_line.set_data([], [])
                return (f_true_line, Y_in_points, f_out_line, *legend)

            start_time_ns = time.time_ns()
            ani = animation.FuncAnimation(  # pylint: disable=unused-variable
                fig,
                animate,
                np.arange(args.train_steps),
                interval=20,
                blit=True,
                init_func=init,
                repeat=False,
            )
            plt.show()
            end_time_ns = time.time_ns()

            training_time_s = (end_time_ns - start_time_ns) / 1e9
            print(f'Training time: {training_time_s:.3g} s')


def _format_losses(losses):
    return (
        f"LogLikelihood={losses.log_likelihood:.3g}, "
        f"DataFit={losses.data_fit:.3g}, "
        f"Complexity={losses.complexity:.3g}")


def _evolve_lorenz():
    np.random.seed(1)
    sigma = 10
    beta = 8 / 3
    rho = 28

    def evol_func(t, y):
        del t
        x, y, z = y
        dxdt = sigma * (y - x)
        dydt = x * (rho - z) - y
        dzdt = x * y - beta * z
        return dxdt, dydt, dzdt

    return scipy.integrate.solve_ivp(
        fun=evol_func,
        t_span=(0, _T_MAX_TEST),
        y0=10 * np.random.normal(size=(3, )),
        dense_output=True,
    ).sol


@attr.dataclass()
class _Trainer:
    model: gps.Model
    train_op: typing.Any
    session: util.SessionWrapper
    data_points: int

    def step(self, i):
        sol = _evolve_lorenz()

        X_in = np.random.uniform(
            low=_T_MIN, high=_T_MAX_TRAIN, size=self.data_points)
        f = sol(X_in)[_TRAINING_VAR_INDEX]
        Y_in = f + np.random.normal(scale=0.6, size=self.data_points)

        losses, _ = self.session.run(
            [self.model.losses, self.train_op],
            feed_dict={
                self.model.X_in: X_in[:, np.newaxis],
                self.model.Y_in: Y_in,
            },
            summaries=True,
        )
        print(f"Step {i}: {_format_losses(losses)}")

        f_out = self.session.run(
            self.model.f_out_mean,
            feed_dict={
                self.model.X_in: X_in[:, np.newaxis],
                self.model.Y_in: Y_in,
                self.model.X_out: _T_GRID[:, np.newaxis],
            },
        )

        print('Variables:')
        util.print_variables(self.session)

        return sol, X_in, Y_in, f_out


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
