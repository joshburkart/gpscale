"""2D example.

To run:

```python
PYTHONPATH=. python3 examples/simple_2d.py
```
"""

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import gpscale as gps

_EPSILON = 1e-4
_LENGTH_SCALE = 5


def main():
    np.random.seed(10)
    X_in = np.random.uniform(0, 100, (3, 2))
    Y_in = np.array([10., 10., 1.])

    f_out_mean = _run(X_in, Y_in, 500)

    fig, ax = plt.subplots()
    ax.set(xlim=[0, 100], ylim=[0, 100])
    res = ax.imshow(f_out_mean.T[::-1], extent=(0, 100, 0, 100))
    plt.colorbar(res, ax=ax)
    ax.scatter(*X_in.T, facecolors='none', edgecolors='red')
    fig.tight_layout()
    plt.show()


def _run(X_in, Y_in, num_grid_points):
    with tf.Graph().as_default():  # pylint: disable=not-context-manager
        linspace_args = (0., 100., num_grid_points)
        grid = gps.Grid1D(*linspace_args)
        product_grid = gps.ProductGrid((grid, ) * 2)

        kernel = gps.kernels.RBF(length_scale=_LENGTH_SCALE)
        kernel = gps.kernels.TensorProduct((kernel, ) * 2)
        kernel *= gps.kernels.Scalar(10.**2)
        kernel = kernel.make_grid_interp_kernel(product_grid)  #

        x = np.linspace(*linspace_args)
        y = np.linspace(*linspace_args)
        x[0] += _EPSILON
        x[-1] -= _EPSILON
        y[0] += _EPSILON
        y[-1] -= _EPSILON

        X_out = np.stack(np.meshgrid(x, y, indexing='ij'), axis=-1)
        f_out_shape = X_out.shape[:2]
        X_out = X_out.reshape((-1, 2))

        f_out_mean = gps.Model.build(
            X_in=X_in,
            X_out=X_out,
            Y_in=Y_in,
            kernel=kernel,
            noise_kernel=gps.kernels.WhiteNoise(10.),
            mean_func=gps.ConstantMean(4.),
            solve_params=dict(verbose=True, max_iter=10),
        ).f_out_mean
        f_out_mean = tf.reshape(f_out_mean, f_out_shape)

        with tf.Session() as session:
            session.run(tf.global_variables_initializer())
            f_out_mean = session.run(f_out_mean)

    return f_out_mean


if __name__ == '__main__':
    main()
