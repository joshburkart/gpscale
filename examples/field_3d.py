"""3D "geospatial" example.

To run:

```
OUTPUT_PATH=~/Desktop/field_3d.mp4 && \
PYTHONPATH=. time python3 examples/field_3d.py \
  --output_anim_path="$OUTPUT_PATH" \
&& \
open "$OUTPUT_PATH"
```
"""

import typing

import attr
import click
import numpy as np
import tensorflow as tf
from matplotlib import animation
from matplotlib import pyplot as plt

import gpscale as gps
from examples import util

_T_PLOT_POINTS = 50
_XY_PLOT_POINTS = 100


@click.command()
@click.option('--output_anim_path', type=str)
@click.option('--profile/--no-profile', default=False)
@click.option('--t_points', default=50, type=int)
@click.option('--xy_points', default=100, type=int)
@click.option('--max_iter', default=100, type=int)
@util.Args.decorate
def main(args):
    params = _make_params(args)

    t, x, y, f_interp = _run(params=params)

    print('Plotting results...')
    if args.output_anim_path is not None:
        _make_plots(
            params, t, x, y, f_interp, output_path=args.output_anim_path)


def _make_params(args):
    args = vars(args)
    param_kwargs = {}
    for arg_name, param_name in [
        ('xy_points', 'x_points'),
        ('xy_points', 'y_points'),
        ('t_points', 't_points'),
        ('max_iter', 'max_iter'),
        ('profile', 'profile'),
    ]:
        if args[arg_name] is not None:
            param_kwargs[param_name] = args[arg_name]

    return _Params(**param_kwargs)


def _run(params: '_Params'):
    np.random.seed(1)

    X_in = np.stack(
        [
            np.random.uniform(*params.t_bounds, params.n_in),
            np.random.uniform(*params.x_bounds, params.n_in),
            np.random.uniform(*params.y_bounds, params.n_in),
        ],
        axis=1)
    f_in = _make_synth_data(params, *X_in.T)
    Y_in = f_in + np.random.normal(scale=params.noise_std_dev, size=f_in.shape)
    with tf.Graph().as_default():  # pylint: disable=not-context-manager
        product_grid = gps.ProductGrid(
            (
                gps.Grid1D(*params.t_bounds, params.t_points),
                gps.Grid1D(*params.x_bounds, params.x_points),
                gps.Grid1D(*params.y_bounds, params.y_points),
            ))

        xy_length_scale = 4.
        t_kernel = gps.kernels.RBF(0.8)
        xy_kernel = gps.kernels.RBF(xy_length_scale)

        kernel = gps.kernels.TensorProduct((t_kernel, xy_kernel, xy_kernel))
        kernel *= gps.kernels.Scalar(10.**2)
        kernel = kernel.make_grid_interp_kernel(product_grid, use_ffts=False)

        t = np.linspace(*params.t_bounds, _T_PLOT_POINTS)
        x = np.linspace(*params.x_bounds, _XY_PLOT_POINTS)
        y = np.linspace(*params.y_bounds, _XY_PLOT_POINTS)

        X_out = np.stack(np.meshgrid(t, x, y, indexing='ij'), axis=-1)
        f_out_shape = X_out.shape[:3]
        X_out = X_out.reshape((-1, 3))

        print('Building graph...')
        f_out = gps.Model.build(
            X_in=X_in,
            X_out=X_out,
            Y_in=Y_in,
            kernel=kernel,
            noise_kernel=gps.kernels.WhiteNoise(params.noise_std_dev**2),
            mean_func=gps.ConstantMean(20.),
            solve_params=dict(
                max_iter=params.max_iter,
                verbose=True,
                tol=1e-2,
                enforce_tol=False),
        ).f_out_mean
        f_out = tf.reshape(f_out, f_out_shape)

        with util.make_session(profile=params.profile) as session:
            session.run(tf.global_variables_initializer())

            print('Variables:')
            util.print_variables(session)

            print('Running...')
            f_out = session.run(f_out)

    return t, x, y, f_out


def _make_plots(params: '_Params', t, x, y, f_interp, output_path: str):
    fig, axes = plt.subplots(1, 2)
    axes = axes.flat

    f_true = _make_synth_data(params, *np.meshgrid(t, x, y, indexing='ij'))

    def imshow(ax, frame):
        return ax.imshow(
            frame,
            animated=True,
            vmin=params.mean - 1.3 * params.front_amp,
            vmax=params.mean + 1.3 * params.front_amp)

    axes[0].set(title='Input')
    axes[1].set(title='Interpolation')

    im_true = imshow(axes[0], f_true[0])
    im_interp = imshow(axes[1], f_interp[0])

    def make_frame(frames):
        f_true_frame, f_interp_frame = frames
        im_true.set_array(f_true_frame)
        im_interp.set_array(f_interp_frame)

        return im_true, im_interp

    fig.set_size_inches(10, 4)
    axes[0].set(aspect='auto')
    axes[1].set(aspect='auto')
    plt.colorbar(im_true, ax=axes[0])
    plt.colorbar(im_interp, ax=axes[1])

    fig.tight_layout()

    f_anim = animation.FuncAnimation(
        fig, make_frame, frames=zip(f_true, f_interp), blit=True)
    f_anim.save(output_path, writer='ffmpeg', dpi=300)


def _make_synth_data(params: '_Params', t, x, y):
    fluct = np.sin(2 * np.pi * x / params.fluct_xy_scale)
    fluct = fluct * np.sin(2 * np.pi * y / params.fluct_xy_scale)
    fluct = fluct * np.cos(2 * np.pi * t / params.fluct_t_scale)
    fluct *= params.fluct_amp

    front_arg = (
        (
            0.5 * (x - params.x_mid) +  #
            2. * (y - params.y_mid)  #
        ) / params.front_xy_scale - t * params.front_speed)
    front = params.front_amp * np.tanh(front_arg)

    return params.mean + fluct + front


@attr.dataclass()
class _Params:
    epsilon: float = 1e-5

    max_iter: int = 50
    profile: bool = False

    n_in: int = int(1e4)

    t_bounds: typing.List[float] = [0., 1.]
    x_bounds: typing.List[float] = [10., 110.]
    y_bounds: typing.List[float] = [-10., 90.]

    t_points: int = 20
    x_points: int = 20
    y_points: int = 20

    mean: float = 20.

    fluct_xy_scale: float = 20.
    fluct_t_scale: float = 0.8
    fluct_amp: float = 3.

    front_xy_scale: float = 10.
    front_speed: float = 3.
    front_amp: float = 10.

    noise_std_dev: float = 2.

    @property
    def x_mid(self):
        return sum(self.x_bounds) / 2.

    @property
    def y_mid(self):
        return sum(self.y_bounds) / 2.


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
