"""TensorFlow utilities for examples.

Easy ways to run TensorBoard, run with profiling, etc.
"""

import contextlib
import subprocess
import tempfile
import threading
import time
import traceback
import typing as t
import webbrowser

import attr
import numpy as np
import tensorflow as tf


class Args:
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    @classmethod
    def decorate(cls, main_):
        def new_main(**kwargs):
            args = cls(**kwargs)
            main_(args)

        return new_main


@contextlib.contextmanager
def make_session(profile=False, xla=False, tensorboard=False):
    if xla:
        # Config to turn on JIT XLA compilation.
        config = tf.ConfigProto()
        config.graph_options.optimizer_options.global_jit_level = (  # pylint: disable=no-member
            tf.OptimizerOptions.ON_1)  # pylint: disable=no-member
    else:
        config = None

    with tf.Session(config=config) as session:
        with tempfile.TemporaryDirectory() as temp_dir:
            try:
                file_writer = tf.summary.FileWriter(temp_dir, session.graph)
                if tensorboard:
                    tensorboard_process, tensorboard_opener_thread = _launch_tensorboard(
                        temp_dir)

                with file_writer:
                    if profile:
                        run_metadata = tf.RunMetadata()
                        options = tf.RunOptions(
                            trace_level=tf.RunOptions.FULL_TRACE)  # pylint: disable=no-member
                        profiler = tf.profiler.Profiler(session.graph)
                    else:
                        run_metadata = None
                        options = None
                        profiler = None

                    merged_summary = tf.summary.merge_all()

                    yield SessionWrapper(
                        session=session,
                        merged_summary=merged_summary,
                        options=options,
                        run_metadata=run_metadata,
                        file_writer=file_writer,
                        profiler=profiler)
            except:
                traceback.print_exc()
                print('Exception raised...')
                raise
            finally:
                if tensorboard:
                    input('tensorboard still running; <enter> to finish/kill ')
                    tensorboard_process.terminate()
                    tensorboard_opener_thread.join()
                    print('Killed tensorboard')

    if profile:
        option_builder = tf.profiler.ProfileOptionBuilder
        opts = (
            option_builder(option_builder.time_and_memory())  #
            .with_step(-1)  #
            .with_stdout_output()  #
            .select(['micros', 'bytes', 'occurrence'])  #
            .order_by('micros')  #
            .build()  #
        )
        profiler.profile_operations(options=opts)


def add_trainable_summaries():
    with tf.name_scope('Variables'):
        for variable in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES):
            shape = list(variable.get_shape())
            if len(shape) == 0:
                tf.summary.scalar(variable.name, variable)
            elif len(shape) == 1:
                if shape[0] == 1:
                    tf.summary.scalar(variable.name, variable[0])
                else:
                    for dim in range(shape[0]):
                        tf.summary.scalar(
                            f'{variable.name}/{dim}', variable[dim])


def add_loss_summaries(losses):
    with tf.name_scope('Losses'):
        tf.summary.scalar('log_likelihood', losses.log_likelihood)
        tf.summary.scalar('complexity', losses.complexity)
        tf.summary.scalar('data_fit', losses.data_fit)


def print_variables(session, indent_chars=4):
    variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    for variable in sorted(variables, key=lambda variable: variable.name):
        value = session.run(variable)
        formatted = np.array2string(value, precision=3)
        print(' ' * indent_chars + f'{variable.name[:-2]}: {formatted}')


@attr.dataclass(kw_only=True, frozen=False)
class SessionWrapper:
    session: tf.Session
    merged_summary: tf.Tensor
    step: int = 0

    file_writer: tf.summary.FileWriter

    options: t.Optional[tf.RunOptions]
    run_metadata: t.Optional[tf.RunMetadata]
    profiler: t.Optional[tf.profiler.Profiler]

    def run(self, fetch, summaries=False, **kwargs):
        if summaries:
            was_list = True
            if not isinstance(fetch, list):
                was_list = False
                fetch = [fetch]
            result = self.session.run([self.merged_summary] + fetch, **kwargs)
            self.file_writer.add_summary(result[0], self.step)
            self.step += 1

            if was_list:
                return result[1:]
            return result[1]
        return self.session.run(fetch, **kwargs)

    def run_with_profile(self, fetch, **kwargs):
        result = self.run(
            fetch,
            run_metadata=self.run_metadata,
            options=self.options,
            **kwargs)
        self.file_writer.add_run_metadata(
            self.run_metadata, tag=str(self.step))
        self.profiler.add_step(self.step, self.run_metadata)
        return result


def _launch_tensorboard(
        temp_dir: str,
) -> t.Tuple[subprocess.Popen, threading.Thread]:
    tensorboard_args = [
        'tensorboard',
        f'--logdir={temp_dir}',
        '--host=127.0.0.1',
        '--port=6006',
    ]
    print(f'Launching tensorboard: {tensorboard_args}')
    process = subprocess.Popen(
        args=tensorboard_args, stderr=subprocess.DEVNULL)

    def open_in_browser():
        time.sleep(5)
        webbrowser.open_new_tab('http://127.0.0.1:6006')

    opener_thread = threading.Thread(target=open_in_browser)
    opener_thread.start()

    return process, opener_thread
