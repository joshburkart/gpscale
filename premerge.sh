#!/bin/bash

cd $(dirname "$0")
export PYTHONPATH=gpscale

set -xe

yapf -d --recursive gpscale/
python3 -m pylint gpscale/
python3 -m pytest --tb=native gpscale/
