"""Test fixtures for `pytest`.

This special file is automatically imported by `pytest`. (Pretty stupid way of
doing things if you ask me.)
"""

import pytest
import tensorflow as tf

from . import test_util


@pytest.fixture()
def tf_session():
    graph = tf.Graph()
    with graph.as_default():  # pylint: disable=not-context-manager
        tf.set_random_seed(1)
        with tf.Session() as session:
            yield test_util.SessionWrapper(session)
