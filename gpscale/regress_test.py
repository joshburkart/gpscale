import numpy as np
import pytest
import tensorflow as tf

from . import gridding
from . import hyper
from . import kernels
from . import regress
from . import util


@pytest.mark.parametrize('dtype', [tf.float32, tf.float64])
@pytest.mark.parametrize('grid_interp', [True, False])
def test_predict(tf_session, grid_interp, dtype):
    np.random.seed(1)

    td = _TestData(grid_interp, dtype)
    f_out = regress.Model.build(
        X_in=td.X_in,
        Y_in=td.Y_in,
        X_out=td.X_out,
        kernel=td.kernel,
        noise_kernel=kernels.WhiteNoise(td.noise_var_scale),
        solve_params=dict(max_iter=100),
    ).f_out_mean
    f_out_np = tf_session.run(f_out)

    # import matplotlib.pyplot as plt
    # plt.scatter(X_in_np, Y_in_np, s=0.5)
    # plt.scatter(X_out_np, f_out_np, s=0.5, alpha=0.5)
    # plt.show()

    expected_f_out_np = np.sqrt(td.var_scale) * np.sin(
        td.X_out_np / td.length_scale)
    np.testing.assert_allclose(
        f_out_np, expected_f_out_np, atol=3 * np.sqrt(td.noise_var_scale))


@pytest.mark.parametrize('dtype', [tf.float32, tf.float64])
def test_log_likelihood_grid_vs_no(tf_session, dtype):
    with tf.variable_scope('NoGrid'):
        td = _TestData(grid_interp=False, dtype=dtype)
        log_likelihood = regress.Model.build(
            X_in=td.X_in,
            Y_in=td.Y_in,
            kernel=td.kernel,
            noise_kernel=kernels.WhiteNoise(td.noise_var_scale),
            solve_params=dict(max_iter=50, enforce_tol=False),
            det_params=dict(
                stoch_trace_params=dict(num_probes=100),
                lanczos_params=dict(max_order=10),
            ),
        ).losses.log_likelihood
        log_likelihood_np_no_grid = tf_session.run(log_likelihood)
    with tf.variable_scope('Grid'):
        td = _TestData(grid_interp=True, dtype=dtype)
        log_likelihood = regress.Model.build(
            X_in=td.X_in,
            Y_in=td.Y_in,
            kernel=td.kernel,
            noise_kernel=kernels.WhiteNoise(td.noise_var_scale),
            solve_params=dict(max_iter=50, enforce_tol=False),
            det_params=dict(
                stoch_trace_params=dict(num_probes=100),
                lanczos_params=dict(max_order=10),
            ),
        ).losses.log_likelihood
        log_likelihood_np_grid = tf_session.run(log_likelihood)

    np.testing.assert_allclose(
        log_likelihood_np_grid, log_likelihood_np_no_grid, rtol=1e-1)


class _TestData:  # pylint: disable=too-many-instance-attributes
    def __init__(self, grid_interp: bool, dtype: tf.DType):
        np.random.seed(1)

        self.n_in = 50
        self.n_out = 100

        self.var_scale = 234.
        self.length_scale = 6.7
        self.noise_var_scale = 0.1

        self.X_in_np = np.random.uniform(0, 20 * np.pi, self.n_in)
        self.Y_in_np = (
            np.sqrt(self.var_scale) * np.sin(self.X_in_np / self.length_scale)
            + np.sqrt(self.noise_var_scale) * np.random.normal(size=self.n_in))
        self.X_out_np = np.random.uniform(0, 20 * np.pi, self.n_out)

        self.X_in = util.ensure_tensor(self.X_in_np, preferred_dtype=dtype)
        self.Y_in = util.ensure_tensor(self.Y_in_np, preferred_dtype=dtype)
        self.X_out = util.ensure_tensor(self.X_out_np, preferred_dtype=dtype)

        self.kernel = kernels.Gaussian(
            length_scale=hyper.positive(self.length_scale))
        self.kernel = kernels.Scalar(self.var_scale) * self.kernel
        if grid_interp:
            grid = gridding.Grid1D(0, 29 * np.pi, 400)
            self.kernel = self.kernel.make_grid_interp_kernel(grid)


def test_log_likelihood_exact(tf_session):
    X_in = [0., 1.]
    Y_in = [2., 2.]
    kernel = kernels.Gaussian()

    losses = regress.Model.build(
        X_in=X_in,
        Y_in=Y_in,
        kernel=kernel,
        noise_kernel=kernels.WhiteNoise(0.),
        det_params=dict(
            lanczos_params=dict(),
            stoch_trace_params=dict(num_probes=1000),
        ),
    ).losses
    losses = tf_session.run(losses)
    np.testing.assert_allclose(
        losses.data_fit, -4 + 4 / (1 + np.sqrt(np.e)), atol=5e-3)
    np.testing.assert_allclose(
        losses.complexity, -np.log(1 - 1 / np.e) / 2, atol=0.06)


if __name__ == '__main__':
    pytest.main()
