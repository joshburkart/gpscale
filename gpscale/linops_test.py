import numpy as np
import pytest
import tensorflow as tf

from . import gridding
from . import kernels
from . import linops
from . import util


def test_matrix_shape(tf_session):
    matrix_op = linops.DenseMatrixOp(
        util.ensure_tensor([
            [1., 0.],
            [3., 3.],
        ], ensure_dtype_is=tf.float32))
    assert matrix_op.dtype == tf.float32

    _test_shapes_op_combinations_impl(tf_session, matrix_op)


def test_conv_shape(tf_session):
    conv_op = linops.ValidSymmConvOp(
        util.ensure_tensor(np.ones(5), ensure_dtype_is=tf.float32))
    assert conv_op.dtype == tf.float32

    _test_shapes_op_combinations_impl(tf_session, conv_op)


def test_gridded_shape(tf_session):
    grid = gridding.Grid1D(0., 1., 10)
    product_grid = gridding.ProductGrid((grid, ) * 2)
    tensor_prod_kernel = kernels.TensorProduct((kernels.RBF(), ) * 2)
    X = util.ensure_tensor([[0.25, 0.5]] * 2, ensure_dtype_is=tf.float64)
    gridded_op = (
        tensor_prod_kernel  #
        .make_grid_interp_kernel(product_grid)  #
        .make_lin_op(X)  #
    )
    assert gridded_op.dtype == tf.float64

    _test_shapes_op_combinations_impl(tf_session, gridded_op)


def _test_shapes_op_combinations_impl(tf_session, op: linops.LinOp):
    a = linops.ScalarOp(util.ensure_tensor(5., ensure_dtype_is=op.dtype))

    _test_shapes_impl(tf_session, op)

    _test_shapes_impl(tf_session, op * op)
    _test_shapes_impl(tf_session, op * op * op)

    _test_shapes_impl(tf_session, op + op)
    _test_shapes_impl(tf_session, op + op + op)

    _test_shapes_impl(tf_session, op * (op + a * op))
    _test_shapes_impl(tf_session, (op + a * op) * op)


def _test_shapes_impl(tf_session, lin_op: linops.LinOp):
    vector = tf.random_normal([lin_op.shape[1]], dtype=lin_op.dtype)
    result = lin_op * vector
    assert tf_session.run(result).shape == (tf_session.run(lin_op.shape[0]), )


if __name__ == '__main__':
    pytest.main()
