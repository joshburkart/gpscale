import numpy as np
import pytest
import tensorflow as tf

from . import gridding
from . import linalg
from . import linops as l
from . import util
from . import kernels

# Useful to test private methods, since there's a lot of tricky internal stuff.
# pylint: disable=protected-access


@pytest.mark.parametrize('n', [2, 6, 20, 100])
def test_lanczos_full(tf_session, n):
    """Test where `max_order` = dim of matrix."""
    np.random.seed(1)
    L = np.random.normal(size=(n, n))
    A = L @ L.T
    vector = util.ensure_tensor(np.random.normal(size=n))

    lin_op = l.DenseMatrixOp(util.ensure_tensor(A))
    result = linalg.lanczos(lin_op, vector, params=dict(max_order=n))
    Q, T = tf_session.run([result.Q, result.make_T()])

    _assert_allclose(Q @ Q.T, np.eye(n))
    _assert_allclose(Q.T @ Q, np.eye(n))
    _assert_allclose(Q @ T @ Q.T, A)


@pytest.mark.parametrize('n', [2, 6, 20, 100])
def test_lanczos_partial(tf_session, n):
    """Test where `max_order` < dim of matrix."""
    np.random.seed(1)
    L = np.random.normal(size=(n, n))
    A = L @ L.T
    vector = util.ensure_tensor(np.random.normal(size=n))

    m = min(6, n // 2)

    lin_op = l.DenseMatrixOp(util.ensure_tensor(A))
    result = linalg.lanczos(lin_op, vector, params=dict(max_order=m))
    Q, T = tf_session.run([result.Q, result.make_T()])

    _assert_allclose(Q.T @ Q, np.eye(m))
    _assert_allclose((A @ Q)[:, :-1], (Q @ T)[:, :-1])


@pytest.mark.parametrize('subspace_size_factor', [1 / 2, 3 / 4, 1])
@pytest.mark.parametrize('max_order', [2, 5, 50])
def test_lanczos_invariant(tf_session, max_order, subspace_size_factor):
    """Test detection of invariant subspaces."""
    n = int(max_order * subspace_size_factor)

    np.random.seed(1)
    L = np.random.normal(size=(n, n))
    A = L @ L.T
    vector = util.ensure_tensor(np.random.normal(size=n))

    lin_op = l.DenseMatrixOp(util.ensure_tensor(A))
    result = linalg.lanczos(lin_op, vector, params=dict(max_order=max_order))
    Q, T = tf_session.run([result.Q, result.make_T()])

    assert T.shape == A.shape
    assert Q.shape == A.shape

    _assert_allclose(Q.T @ Q, np.eye(n))
    _assert_allclose(Q @ T @ Q.T, A)


def test_orthogonalize(tf_session):
    basis = np.eye(4)[:3]
    basis = tf.TensorArray(
        dtype=tf.float64, size=3, clear_after_read=False).unstack(basis)
    vector = np.random.normal(size=4)
    vector[3] = 2.
    orth, norm = tf_session.run(linalg._orthogonalize(3, basis, vector))
    np.testing.assert_allclose(orth, [0, 0, 0, 2])
    np.testing.assert_allclose(norm, 2.)


def test_log_det_conv(tf_session):
    K = _make_conv_op(200)

    scalar = l.ScalarOp(util.ensure_tensor(1e-2, ensure_dtype_is=K.dtype))

    with tf.variable_scope('Ks'):
        _test_log_det(tf_session, K + scalar)
    with tf.variable_scope('Kss'):
        _test_log_det(tf_session, K + scalar + scalar)
    with tf.variable_scope('KKs'):
        _test_log_det(tf_session, K + K + scalar)


def test_log_det_matrix(tf_session):
    np.random.seed(1)

    n = 5
    L = np.random.uniform(size=(n, n))
    # Make positive definite.
    A = util.ensure_tensor(
        L @ L.T + 1e-2 * np.eye(n), ensure_dtype_is=tf.float64)
    K = l.DenseMatrixOp(A)

    scalar = l.ScalarOp(util.ensure_tensor(1e-2, ensure_dtype_is=K.dtype))

    with tf.variable_scope('K'):
        _test_log_det(tf_session, K)
    with tf.variable_scope('KK'):
        _test_log_det(tf_session, K + K)
    with tf.variable_scope('KKs'):
        _test_log_det(tf_session, K + K + scalar)
    with tf.variable_scope('KKsss'):
        _test_log_det(tf_session, K + K + scalar + scalar + scalar)


def test_log_det_tensor_prod(tf_session):
    K = _make_conv_op(40)
    scalar = l.ScalarOp(util.ensure_tensor(1e-1, ensure_dtype_is=K.dtype))
    K = l.TensorProductOp((K, K + K)) + scalar

    _test_log_det(tf_session, K)


def test_log_det_grid_interp(tf_session):
    kernel = kernels.Gaussian()
    grid = gridding.Grid1D(0., 5., 1000)
    kernel = kernel.make_grid_interp_kernel(grid)

    X = tf.linspace(0., 5., 300)
    K = kernel.make_lin_op(X)
    K += l.ScalarOp(util.ensure_tensor(1e-1, ensure_dtype_is=K.dtype))

    _test_log_det(tf_session, K)


def _make_conv_op(n):
    grid = tf.cast(tf.linspace(0., 5., n), dtype=tf.float64)
    half_kernel_vec = tf.exp(-grid**2 / 2)
    kernel_vec = tf.concat(
        [half_kernel_vec[::-1], half_kernel_vec[1:]], axis=0)
    return l.ValidSymmConvOp(kernel_vec)


def _test_log_det(tf_session, lin_op: l.LinOp):
    _test_log_det_method(tf_session, lin_op, linalg.DetMethod.SCALED_EVAL)
    _test_log_det_method(tf_session, lin_op, linalg.DetMethod.STOCH_LANCZOS)


def _test_log_det_method(
        tf_session, lin_op: l.LinOp, method: linalg.DetMethod):
    log_det = linalg.log_det(
        lin_op,
        params=dict(
            method=method,
            stoch_trace_params=dict(num_probes=int(10)),
            lanczos_params=dict(max_order=10),
        ),
    )
    tf_session.run(tf.global_variables_initializer())
    expected_log_det = _compute_expected_log_det(tf_session, lin_op)
    log_det = tf_session.run(log_det)

    np.testing.assert_allclose(log_det, expected_log_det, rtol=2e-1)


def _compute_expected_log_det(tf_session, lin_op):
    A = tf.map_fn(lin_op.apply, tf.eye(lin_op.shape[0], dtype=lin_op.dtype))
    A = tf_session.run(A)
    return np.linalg.slogdet(A)[1]


def test_log_det_stoch_lanczos_deriv(tf_session):
    A_diag_init = np.array([1., 2.], dtype=np.float32)
    A_diag = tf.get_variable(
        name='A_diag', initializer=A_diag_init, dtype=tf.float32)
    A = l.DenseMatrixOp(tf.diag(A_diag))
    value_tens = linalg.log_det(
        A,
        params=dict(
            method=linalg.DetMethod.STOCH_LANCZOS,
            stoch_trace_params=dict(num_probes=10),
            lanczos_params=dict(max_order=2),
        ),
    )
    grad_tens, = tf.gradients(value_tens, A_diag)

    value, grad = tf_session.run([value_tens, grad_tens])
    np.testing.assert_allclose(value, np.log(2), rtol=1e-3)
    np.testing.assert_allclose(grad, 1 / A_diag_init, rtol=1e-3)


def test_diag_matrix(tf_session):
    band_matrix = linalg._make_band_matrix({0: [1, 2]})
    np.testing.assert_equal(tf_session.run(band_matrix), [[1, 0], [0, 2]])


def test_band_diag_matrix(tf_session):
    band_matrix = linalg._make_band_matrix(
        {
            0: np.array([1., 2., 7.], dtype=np.float64),
            1: np.array([3., 4.], dtype=np.float64),
            -2: np.array([99.], dtype=np.float64),
        })
    band_matrix = tf_session.run(band_matrix)
    assert band_matrix.dtype == np.float64
    np.testing.assert_allclose(
        band_matrix,
        [
            [1., 3., 0.],
            [0., 2., 4.],
            [99., 0., 7.],
        ],
    )


def test_persistent_rand_static_shape(tf_session):
    blah = tf.placeholder(shape=(3, ), dtype=tf.float32)
    rand = tf.random_uniform(shape=tf.shape(blah))
    rand_persist = linalg._make_persistent_random_var(rand)
    assert rand.get_shape() == rand_persist.get_shape()

    assert not np.allclose(tf_session.run(rand_persist), 0.)
    assert tf_session.run(rand_persist).shape == (3, )
    tf_session.run_init = False
    np.testing.assert_allclose(
        tf_session.run(rand_persist), tf_session.run(rand_persist))


def test_persistent_rand_dyn_shape(tf_session):
    """Test `_make_persistent_random_var` when random tensor has a non-static shape."""
    tf_session.run_init = False

    placeholder = tf.placeholder(dtype=tf.float32)  # No shape specified.
    rand = tf.random_uniform(shape=tf.shape(placeholder))
    rand_persist = linalg._make_persistent_random_var(rand)
    assert rand.get_shape() == rand_persist.get_shape()

    # TODO: Right now the persistent random tensor doesn't get updated if the
    # input's shape changes...
    init_feed_dict = {placeholder: [1., 2.]}
    tf_session.run(tf.global_variables_initializer(), feed_dict=init_feed_dict)

    feed_dict = {placeholder: [1., 2., 3.]}
    assert not np.allclose(
        tf_session.run(rand_persist, feed_dict=feed_dict), 0.)
    assert tf_session.run(rand_persist, feed_dict=feed_dict).shape == (2, )
    np.testing.assert_allclose(
        tf_session.run(rand_persist, feed_dict=feed_dict),
        tf_session.run(rand_persist, feed_dict=feed_dict))


def test_rademacher(tf_session):
    rademacher = linalg._make_rademacher_vectors([50, 50], dtype=tf.int32)
    rademacher = tf_session.run(rademacher)
    assert 1 in rademacher
    assert -1 in rademacher
    assert np.all((rademacher == 1) | (rademacher == -1))


def test_inv_quad_form_1(tf_session):
    """Test the case where $A$ has `Variable`s."""
    x = util.ensure_tensor([1., 1.], ensure_dtype_is=tf.float32)
    A_diag_init = np.array([1., 2.], dtype=np.float32)
    A_diag = tf.get_variable(
        name='A_diag', initializer=A_diag_init, dtype=tf.float32)
    A = l.DenseMatrixOp(tf.diag(A_diag))
    Ainv_x = linalg.solve(A, x, params=None)
    value_tens = linalg.inv_quad_form(A, x, Ainv_x)
    grad_tens, = tf.gradients(value_tens, A_diag)

    value, grad = tf_session.run([value_tens, grad_tens])
    np.testing.assert_allclose(value, 3. / 2)
    np.testing.assert_allclose(grad, -1 / A_diag_init**2)


def test_inv_quad_form_2(tf_session):
    """Test the case where $x$ has `Variable`s."""
    x_init = np.array([1., 2.], dtype=np.float32)
    x = tf.get_variable(name='x', initializer=x_init, dtype=tf.float32)
    A = l.DenseMatrixOp(tf.eye(2, dtype=tf.float32))
    Ainv_x = linalg.solve(A, x, params=None)
    value_tens = linalg.inv_quad_form(A, x, Ainv_x)
    grad_tens, = tf.gradients(value_tens, x)

    value, grad = tf_session.run([value_tens, grad_tens])
    np.testing.assert_allclose(value, 5.)
    np.testing.assert_allclose(grad, 2. * x_init)


def _assert_allclose(arr_1, arr_2):
    np.testing.assert_allclose(arr_1, arr_2, rtol=1e-4, atol=1e-5)


if __name__ == '__main__':
    pytest.main()
