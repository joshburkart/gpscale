import numpy as np
import pytest
import scipy.signal
import tensorflow as tf

from . import util


@pytest.mark.parametrize('use_fft', [True, False])
@pytest.mark.parametrize('array_n', [1, 2, 4, 100])
@pytest.mark.parametrize('vector_n', [1, 2, 4, 100])
def test_conv_shape(tf_session, use_fft, array_n, vector_n):
    """Ensure valid convolutions produce outputs with expected shapes."""
    vector_np = np.ones(vector_n)
    vector_np /= min(vector_n, array_n)
    array_np = np.ones(array_n)

    expected_conv = np.ones(abs(vector_n - array_n) + 1)

    vector = util.ensure_tensor(vector_np)
    array = util.ensure_tensor(array_np)

    conv_av = util.convolve_1d_valid(array, vector, use_fft=use_fft)
    conv_va = util.convolve_1d_valid(vector, array, use_fft=use_fft)

    conv_av_np = tf_session.run(conv_av)
    conv_va_np = tf_session.run(conv_va)

    np.testing.assert_allclose(conv_av_np, expected_conv, rtol=1e-4)
    np.testing.assert_allclose(conv_va_np, expected_conv, rtol=1e-4)


@pytest.mark.parametrize('use_fft', [True, False])
@pytest.mark.parametrize(
    ['vector_len', 'array_len', 'array_dim'], [
        (2, 5, 1),
        (21, 2, 1),
        (20, 2, 1),
        (801, 2, 1),
        (25, 50, 2),
    ])
def test_conv_axis(tf_session, use_fft, vector_len, array_len, array_dim):
    """Test convolving against multidimensional arrays."""
    np.random.seed(1)
    vector = np.random.uniform(size=(vector_len, ))
    array = np.random.uniform(size=(array_len, ) * array_dim)

    for axis in range(array_dim):
        conv_tens = util.convolve_1d_valid(
            util.ensure_tensor(array),
            util.ensure_tensor(vector),
            axis=axis,
            use_fft=use_fft)
        conv = tf_session.run(conv_tens)

        new_vector_shape = [1] * array_dim
        new_vector_shape[axis] = len(vector)
        vector = vector.reshape(new_vector_shape)

        expected_conv = scipy.signal.convolve(array, vector, mode='valid')

        np.testing.assert_allclose(conv, expected_conv, rtol=1e-3)


@pytest.mark.parametrize(
    "values_to_insert", [
        [-101., 100.],
        [1.1, 7.3],
        [7.3, 1.1],
        [3.],
        [np.infty, 0.],
        np.arange(-40., 40., np.pi),
    ])
def test_searchsorted_normal(tf_session, values_to_insert):
    sorted_array = [-100., 1., 2., 4., 7., 7.5]
    for dtype in np.dtype('float32'), np.dtype('float64'):
        _test_searchsorted_impl(
            tf_session, np.asarray(sorted_array, dtype=dtype),
            np.asarray(values_to_insert, dtype=dtype))


def test_searchsorted_empty(tf_session):
    _test_searchsorted_impl(tf_session, [], [])
    _test_searchsorted_impl(tf_session, [1., 2.], [])
    _test_searchsorted_impl(tf_session, [], [1., 2.])


def test_searchsorted_equal(tf_session):
    tf_searchsorted = tf_session.run(
        util.searchsorted([-np.infty, 0., 1., 2., 3.], [1.]))

    # At the moment we don't guarantee whether left or right insertion will
    # occur when there's exact equality.
    try:
        np.testing.assert_equal(tf_searchsorted, [2])
    except AssertionError:
        np.testing.assert_equal(tf_searchsorted, [3])


def test_searchsorted_dtype(tf_session):
    del tf_session
    for dtype in [tf.int32, tf.int64]:
        indices = util.searchsorted(
            [-np.infty, 0., 1., 2., 3.], [1.], dtype=dtype)
        assert indices.dtype == dtype


def _test_searchsorted_impl(tf_session, sorted_array, values_to_insert):
    tf_searchsorted = tf_session.run(
        util.searchsorted(sorted_array, values_to_insert))
    np_searchsorted = np.searchsorted(sorted_array, values_to_insert)

    np.testing.assert_equal(tf_searchsorted, np_searchsorted)


def test_convert(tf_session):
    del tf_session

    x1 = [[1, 2], [4, 5]]
    x1 = util.ensure_tensor(x1, name='x1', preferred_dtype=None)
    assert x1.name == 'x1:0'
    assert x1.dtype == tf.int32
    assert util.get_rank(x1) == 2

    x2 = util.ensure_tensor(x1)
    assert x2 is x1

    x3 = util.ensure_dtype(x2, tf.float32)
    assert x3.dtype == tf.float32

    x4 = util.ensure_dtype(x3, tf.float32)
    assert x4 is x3

    x5 = util.ensure_dtype(x4, tf.int32)
    assert x5.dtype == tf.int32

    x6 = util.ensure_rank(x5, 2)
    assert x6 is x5

    x7 = util.ensure_rank(x6, 3)
    assert util.get_rank(x7) == 3

    x8 = util.ensure_rank(x7, 2)
    assert util.get_rank(x8) == 2

    with pytest.raises(ValueError):
        util.ensure_rank(x8, 1)


if __name__ == '__main__':
    pytest.main()
