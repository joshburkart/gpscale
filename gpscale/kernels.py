"""GP kernels.

TODO: Don't merge kernel sums/products on creation of `Kernel` objects. Instead,
merge only when constructing `LinOp`s.
"""

import abc
import functools
import typing as t

import numpy as np
import tensorflow as tf

from . import gridding
from . import hyper
from . import linops
from . import util

T = tf.Tensor
D = tf.DType


class Kernel(metaclass=abc.ABCMeta):
    """Interface for kernel functions."""

    @abc.abstractmethod
    def make_lin_op(self, X: T, X_other: T = None) -> linops.LinOp:
        """Produce a covariance `LinOp` from two sets of points.

        Args:
            X: A 2-dimensional `tf.Tensor`.
                Axis 0: Which measurement.
                Axis 1: Which spatial/temporal/etc. axis.
            X_other: If `None`, compute `K(X, X)`. Otherwise compute
                `K(X, X_other)`.
        """

    def _variable_scope(self):
        name = self.__class__.__name__
        name = name.replace('_', '')
        name = name.replace('Kernel', '')
        return tf.variable_scope(name)


class _GriddableMixin:
    """Interface for kernels compatible with structured kernel interpolation.

    See: Wilson, Andrew, and Hannes Nickisch. "Kernel interpolation for scalable
    structured Gaussian processes (KISS-GP)." International Conference on
    Machine Learning. 2015.
    """

    @abc.abstractmethod
    def make_grid_lin_op(
            self,
            grid: gridding.Grid,
            dtype: tf.DType,
            use_ffts: bool = True,
    ) -> linops.LinOp:
        """Make a covariance `LinOp` based on values on a grid.

        Args:
            grid: The grid to apply. Must be a `Grid1D` for `StationaryKernel`
                subclasses, or be a commensurate `ProductGrid` for
                `TensorProduct` kernels.
            use_ffts:
                Whether to use FFTs to compute convolutions (efficient for large
                inputs).
        """

    def make_grid_interp_kernel(
            self,
            grid: gridding.Grid,
            use_ffts: bool = True,
    ) -> '_GridInterpKernel':
        """Apply grid interpolation to this kernel.

        Args:
            grid: The grid to apply. Must be a `Grid1D` for `StationaryKernel`
                subclasses, or be a commensurate `ProductGrid` for
                `TensorProduct` kernels.
            use_ffts:
                Whether to use FFTs to compute convolutions (efficient for large
                inputs).
        """
        return _GridInterpKernel(
            grid=grid,
            inducing_lin_op_factory=functools.partial(
                self.make_grid_lin_op,
                grid=grid,
                use_ffts=use_ffts,
            ),
        )


@util.dataclass(repr=False)
class WhiteNoise(Kernel):
    """White noise kernel."""
    variance: hyper.IntoHyperSpec = hyper.positive(init_value=1.)

    def make_lin_op(self, X: T, X_other: T = None) -> linops.ScalarOp:
        del X_other
        with self._variable_scope():
            variance = hyper.into_tensor(
                self.variance, var_name='variance', dtype=X.dtype)
            return linops.ScalarOp(variance)


class Stationary(Kernel, _GriddableMixin):
    """Interface for stationary kernel functions."""

    def kernel_func(self, delta_X_mag_sq: T) -> T:
        with self._variable_scope():
            return self._kernel_func(util.ensure_tensor(delta_X_mag_sq))

    @abc.abstractmethod
    def _kernel_func(self, delta_X_mag_sq: T) -> T:
        """Override this method to produce a new stationary kernel."""

    def make_lin_op(self, X: T, X_other: T = None) -> linops.DenseMatrixOp:
        """Make a dense covariance matrix linear operator.

        Args:
            X: A `tf.Tensor` with coordinates, of shape
                (<num points>, <num dimensions>).
            X_other: Same as `X`. If not specified, defaults to `X`.
        """
        if X_other is None:
            X_other = X

        if util.get_rank(X) == 1:
            X = X[:, tf.newaxis]
        if util.get_rank(X) != 2:
            raise ValueError(X)
        if util.get_rank(X_other) == 1:
            X_other = X_other[:, tf.newaxis]
        if util.get_rank(X_other) != 2:
            raise ValueError(X_other)

        delta_X = X[:, tf.newaxis, :] - X_other[tf.newaxis, :, :]
        delta_X_mag_sq = tf.reduce_sum(delta_X**2, axis=2)
        kernel_result = self.kernel_func(delta_X_mag_sq)
        if util.get_rank(kernel_result) == 2:
            return linops.DenseMatrixOp(kernel_result)
        if util.get_rank(kernel_result) == 0:
            # Handle scalars.
            return linops.ScalarOp(kernel_result)
        raise ValueError(kernel_result)

    def make_grid_lin_op(
            self,
            grid: gridding.Grid,
            dtype: tf.DType,
            use_ffts: bool = True,
    ) -> linops.ValidSymmConvOp:
        tau = grid.make_tau(dtype=dtype)
        conv_vector_right_half = self.kernel_func(tau**2)
        kernel = tf.concat(
            [conv_vector_right_half[::-1], conv_vector_right_half[1:]], axis=0)
        return linops.ValidSymmConvOp(kernel=kernel, use_fft=use_ffts)

    def __add__(self, other):
        return self._make_binary_op(other=other, binary_op_cls=_StationarySum)

    def __radd__(self, other):
        return self._make_binary_op(
            other=other, binary_op_cls=_StationarySum, reverse=True)

    def __mul__(self, other):
        return self._make_binary_op(
            other=other, binary_op_cls=_StationaryProduct)

    def __rmul__(self, other):
        return self._make_binary_op(
            other=other, binary_op_cls=_StationaryProduct, reverse=True)

    def _make_binary_op(self, other, binary_op_cls, reverse=False):
        if isinstance(other, Stationary):
            # There are custom overrides for things like `_StationarySum +
            # _StationarySum`, so we don't try to handle them here.
            if not (isinstance(self, binary_op_cls)
                    or isinstance(other, binary_op_cls)):
                return binary_op_cls(
                    (self, other) if not reverse else (other, self))
        return NotImplemented


@util.dataclass(repr=False)
class Scalar(Stationary):
    """Scalar variance kernel."""
    variance: hyper.IntoHyperSpec = hyper.positive(init_value=1.)

    @classmethod
    def from_std_dev(cls, std_dev: util.IntoTensor):
        return cls(hyper.positive(init_value=std_dev**2))

    def _kernel_func(self, delta_X_mag_sq: T) -> T:
        return hyper.into_tensor(
            self.variance, var_name='variance', dtype=delta_X_mag_sq.dtype)

    def __mul__(self, other):
        if isinstance(other, Kernel) and not isinstance(other, Stationary):
            return _ScalarProduct(scalar=self, other=other)
        return super().__mul__(other)


@util.dataclass(repr=False)
class _ScalarProduct(Kernel):
    """Special case of a scalar times another kernel."""
    scalar: Scalar
    other: Kernel

    def make_lin_op(self, X: T, X_other: T = None) -> linops.LinOp:
        return (
            self.scalar.make_lin_op(X, X_other) * self.other.make_lin_op(
                X, X_other))


@util.dataclass(repr=False)
class Gaussian(Stationary):
    """Gaussian aka radial basis functino kernel."""
    length_scale: hyper.IntoHyperSpec = hyper.positive(init_value=1.)

    def _kernel_func(self, delta_X_mag_sq: T) -> T:
        length_scale = hyper.into_tensor(
            self.length_scale,
            var_name='length_scale',
            dtype=delta_X_mag_sq.dtype)
        return tf.exp(-delta_X_mag_sq / 2. / length_scale**2)


RBF = Gaussian
SE = Gaussian


@util.dataclass(repr=False)
class Periodic(Stationary):
    """Periodic kernel."""
    spread: hyper.IntoHyperSpec = hyper.positive(init_value=1.)
    period: hyper.IntoHyperSpec = hyper.positive(init_value=1.)

    def _kernel_func(self, delta_X_mag_sq: T) -> T:
        dtype = delta_X_mag_sq.dtype
        spread = hyper.into_tensor(self.spread, var_name='spread', dtype=dtype)
        period = hyper.into_tensor(self.period, var_name='period', dtype=dtype)

        sin_arg = np.pi * tf.sqrt(delta_X_mag_sq) / period
        exponent = -2. * tf.sin(sin_arg)**2 / spread**2
        return tf.exp(exponent)


@util.dataclass(repr=False)
class SpectralMixture(Stationary):
    """Spectral mixture kernel, per Bochner's theorem.

    See: Wilson, Andrew, and Ryan Adams. "Gaussian process kernels for pattern
    discovery and extrapolation." International Conference on Machine Learning.
    2013.

    To construct, likely easiest to use the `make()` factory `classmethod`.
    """
    variances: hyper.IntoHyperSpec
    ang_freqs: hyper.IntoHyperSpec
    length_scales: hyper.IntoHyperSpec

    @classmethod
    def make(
            cls,
            num_components: int,
            init_min_length_scale: float,
            init_max_length_scale: float,
            init_variance_scale: float = 1.,
    ) -> 'SpectralMixture':
        shape = (num_components, )

        sample = functools.partial(
            np.random.uniform,
            size=shape,
            low=1 / init_max_length_scale,
            high=1 / init_min_length_scale)
        ang_freqs_init = 2 * np.pi * sample()
        length_scales_init = sample()
        variances_init = np.random.lognormal(
            size=shape, mean=np.log(init_variance_scale), sigma=2.)

        return cls(
            ang_freqs=hyper.positive(init_value=ang_freqs_init),
            length_scales=hyper.positive(init_value=length_scales_init),
            variances=hyper.positive(init_value=variances_init),
        )

    def _kernel_func(self, delta_X_mag_sq: T) -> T:
        dtype = delta_X_mag_sq.dtype
        rank = util.get_rank(delta_X_mag_sq)

        ang_freqs = hyper.into_tensor(
            self.ang_freqs, var_name='ang_freqs', dtype=dtype)
        length_scales = hyper.into_tensor(
            self.length_scales, var_name='length_scales', dtype=dtype)
        variances = hyper.into_tensor(
            self.variances, var_name='variances', dtype=dtype)

        dX_sq_broadcast = delta_X_mag_sq[..., tf.newaxis]
        reshaper = (tf.newaxis, ) * rank

        exp_arg = -dX_sq_broadcast / length_scales[reshaper]**2
        cos_arg = ang_freqs[reshaper] * tf.sqrt(dX_sq_broadcast)
        components = variances[reshaper] * tf.exp(exp_arg) * tf.cos(cos_arg)
        return tf.reduce_sum(components, axis=rank)


class _TensorProductBase(Kernel, _GriddableMixin):
    """Base for `TensorProduct` kernel and sums thereof."""

    def __add__(self, other) -> Kernel:
        summands = []
        for kernel in self, other:
            if isinstance(kernel, TensorProduct):
                summands.append(kernel)
            elif isinstance(kernel, _SumTensorProduct):
                summands.extend(kernel.summands)
            else:
                raise TypeError
        return _SumTensorProduct(tuple(summands))


@util.dataclass(repr=False)
class TensorProduct(_TensorProductBase):
    """Product of kernels where a different kernel is applied to each axis."""
    axis_kernels: t.Tuple[Stationary, ...]

    def make_lin_op(
            self,
            X: T,
            X_other: t.Optional[T] = None,
    ) -> linops.DenseMatrixOp:
        with self._variable_scope():
            if X_other is None:
                X_other = X

            if not util.get_rank(X) == 2:
                raise ValueError(X)
            if not util.get_rank(X_other) == 2:
                raise ValueError(X_other)

            lin_ops = []
            for axis, kernel in enumerate(self.axis_kernels):
                with tf.variable_scope(str(axis)):
                    lin_ops.append(
                        kernel.make_lin_op(
                            X[:, axis],
                            X_other[:, axis],
                        ))

            return linops.hadamard_product(lin_ops)

    def make_grid_lin_op(
            self,
            grid: gridding.ProductGrid,
            dtype: tf.DType,
            use_ffts: bool = True,
    ) -> linops.TensorProductOp:
        inducing_lin_ops = []
        for i, (axis_grid, kernel) in enumerate(zip(
                grid.axis_grids,
                self.axis_kernels,
        )):
            with tf.variable_scope(str(i)):
                inducing_lin_ops.append(
                    kernel.make_grid_lin_op(
                        axis_grid, dtype=dtype, use_ffts=use_ffts))
        return linops.TensorProductOp(tuple(inducing_lin_ops))

    def __mul__(self, other: Scalar) -> 'TensorProduct':
        if not isinstance(other, Scalar):
            return NotImplemented
        return self.__class__(
            (self.axis_kernels[0] * other, ) + tuple(self.axis_kernels[1:]))

    def __rmul__(self, other: Scalar) -> 'TensorProduct':
        if not isinstance(other, Scalar):
            return NotImplemented
        return self.__class__(
            (other * self.axis_kernels[0], ) + tuple(self.axis_kernels[1:]))


@util.dataclass(repr=False)
class _SumTensorProduct(_TensorProductBase):
    """Sum of `TensorProduct` kernels."""
    summand_kernels: t.Tuple[TensorProduct, ...]

    def make_lin_op(self, X: T, X_other: T = None) -> linops.SumOp:
        return linops.SumOp(
            (
                kernel.make_lin_op(X, X_other)
                for kernel in self.summand_kernels))

    def make_grid_lin_op(
            self,
            grid: gridding.ProductGrid,
            dtype: tf.DType,
            use_ffts: bool = True,
    ) -> linops.SumOp:
        summand_lin_ops = []
        for i, summand in enumerate(self.summand_kernels):
            with tf.variable_scope(str(i)):
                summand_lin_ops.append(
                    summand.make_grid_lin_op(
                        grid, dtype=dtype, use_ffts=use_ffts))
        return linops.SumOp(tuple(summand_lin_ops))


@util.dataclass(repr=False)
class _GridInterpKernel(Kernel):
    grid: gridding.Grid
    # This can't be a `LinOp`, but must instead be a function that produces a
    # `LinOp`, since evaluating a kernel to produce a `LinOp` involves creating
    # TF `Variable`s, which we avoid doing until `_GridInterpKernel.make_lin_op`
    # is called. Otherwise variable scopes aren't handled well (see routines in
    # `regress.py`). This also allows us to use `X.dtype` for the `LinOp`'s
    # `dtype`.
    inducing_lin_op_factory: t.Callable[[tf.DType], linops.LinOp]

    def make_lin_op(
            self,
            X: T,
            X_other: T = None,
    ) -> linops.GridInterpOp:
        with self._variable_scope():
            grid_interp_1 = self.grid.make_grid_interp(X)
            if X_other is None:
                grid_interp_2 = grid_interp_1
            else:
                grid_interp_2 = self.grid.make_grid_interp(X_other)

            return linops.GridInterpOp(
                left_grid_interp=grid_interp_1,
                right_grid_interp=grid_interp_2,
                lin_op=self.inducing_lin_op_factory(dtype=X.dtype),
            )


def _make_sum_base(base_cls):
    """Make a base class for sums that handle `__add__`, etc."""

    class _SumBase(base_cls):
        kernels: t.List[base_cls]

        def __add__(self, other: base_cls):
            if isinstance(other, self.__class__):
                return self.__class__(self.kernels + other.kernels)
            if isinstance(other, base_cls):
                return self.__class__(self.kernels + (other, ))
            return NotImplemented

        def __radd__(self, other: base_cls):
            if isinstance(other, base_cls):
                return self.__class__((other, ) + self.kernels)
            return NotImplemented

    return _SumBase


def _make_product_base(base_cls):
    """Make a base class for products that handles `__mul__`, etc."""

    @util.dataclass(repr=False)
    class _ProductBase(base_cls):
        kernels: t.Tuple[base_cls]

        def __mul__(self, other: base_cls):
            if isinstance(other, self.__class__):
                return self.__class__(self.kernels + other.kernels)
            if isinstance(other, base_cls):
                return self.__class__(self.kernels + (other, ))
            return NotImplemented

        def __rmul__(self, other: base_cls):
            if isinstance(other, base_cls):
                return self.__class__((other, ) + self.kernels)
            return NotImplemented

    return _ProductBase


@util.dataclass(repr=False)
class _StationarySum(_make_sum_base(Stationary)):
    """Sum of stationary kernels."""
    kernels: t.Tuple[Kernel, ...]

    def _kernel_func(self, delta_X_mag_sq: T) -> T:
        return _reduce(
            fns=[kernel.kernel_func for kernel in self.kernels],
            args=(delta_X_mag_sq, ),
            reduce_fn=lambda x, y: x + y)


@util.dataclass(repr=False)
class _StationaryProduct(_make_product_base(Stationary)):
    """Product of stationary kernels."""
    kernels: t.Tuple[Kernel, ...]

    def _kernel_func(self, delta_X_mag_sq: T) -> T:
        return _reduce(
            fns=[kernel.kernel_func for kernel in self.kernels],
            args=(delta_X_mag_sq, ),
            reduce_fn=lambda x, y: x * y)


@util.dataclass(repr=False)
class _Sum(_make_sum_base(Kernel)):
    """Sum of kernels."""
    kernels: t.Tuple[Kernel, ...]

    def make_lin_op(self, X: T, X_other: t.Optional[T] = None) -> linops.LinOp:
        with self._variable_scope():
            return _reduce(
                fns=[kernel.make_lin_op for kernel in self.kernels],
                args=(X, X_other),
                reduce_fn=lambda x, y: x + y)


@util.dataclass(repr=False)
class _Product(_make_product_base(Kernel)):
    """Product of kernels."""
    kernels: t.Tuple[Kernel, ...]

    def make_lin_op(self, X, X_other=None) -> linops.LinOp:
        with self._variable_scope():
            return _reduce(
                fns=[kernel.make_lin_op for kernel in self.kernels],
                args=(X, X_other),
                reduce_fn=lambda x, y: x * y)


def _reduce(fns, args, reduce_fn):
    """Utility function for products and sums."""
    result = None
    for i, fn in enumerate(fns):
        with tf.variable_scope(str(i)):
            kernel_result = fn(*args)
            if result is None:
                result = kernel_result
            else:
                result = reduce_fn(result, kernel_result)
    if result is None:
        raise ValueError
    return result
