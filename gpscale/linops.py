"""Positive semidefinite (PSD) linear operators."""

import abc
import typing as t

import tensorflow as tf

from . import interp
from . import util

T = tf.Tensor
V = tf.Variable
TV = t.Union[T, V]
TVL = t.Union[TV, 'LinOp']


class _SquareShape:
    """Indicator for `LinOp`s that don't have a shape à priori, e.g. scalars."""


Shape = t.Union[T, _SquareShape]


class LinOp(metaclass=abc.ABCMeta):
    """Interface for PSD linear operators."""

    def __mul__(self, other: TVL):
        if isinstance(other, LinOp):
            return _CompositionOp(self, other)
        return self.apply(other)

    def __add__(self, other: 'LinOp') -> 'SumOp':
        return SumOp((self, other))

    @abc.abstractmethod
    def apply(self, vector: TV) -> T:
        """Apply a PSD linear operator to a batch of vectors.

        Args:
            vector: A 1D `tf.Tensor` or `tf.Variable`.

        Returns:
            A transformed vector.
        """

    @property
    @abc.abstractmethod
    def shape(self) -> Shape:
        """Shape of the PSD linear operator."""

    @property
    @abc.abstractmethod
    def dtype(self) -> tf.DType:
        """Numeric data type of the PSD linear operator."""


class AxisLinOp(LinOp):
    """A linear operator that can also be applied to a specific tensor axis."""

    @abc.abstractmethod
    def apply_to_axis(self, x, axis: int):
        pass

    def apply(self, vector):
        if util.get_rank(vector) != 1:
            raise ValueError(vector)
        return self.apply_to_axis(vector, axis=0)

    def __add__(self, other: 'AxisLinOp') -> 'SumAxisOp':
        if isinstance(other, AxisLinOp):
            return SumAxisOp((self, other))
        return super().__add__(other)

    def __mul__(self, other: 'AxisLinOp') -> '_CompositionAxisOp':
        if isinstance(other, AxisLinOp):
            return _CompositionAxisOp(left=self, right=other)
        return super().__mul__(other)


def _make_composition_op_base(base_cls):
    @util.dataclass()
    class _CompositionOpBase(base_cls):
        """Composition of two linear operators."""
        left: base_cls
        right: base_cls

        @property
        def shape(self) -> Shape:
            left_shape = self.left.shape
            right_shape = self.right.shape
            if left_shape is _SquareShape:
                return right_shape
            if right_shape is _SquareShape:
                return left_shape
            return tf.stack([left_shape[0], right_shape[1]], axis=0)

        def log_det(self) -> T:
            left, right = self.left, self.right
            if isinstance(self.right, ScalarOp):
                left, right = right, left
            if isinstance(left, ScalarOp):
                return right.log_det() + right.shape[0] * tf.log(left.value)
            return left.log_det() + right.log_det()

        @property
        def dtype(self) -> tf.DType:
            return self.left.dtype

    return _CompositionOpBase


class _CompositionOp(_make_composition_op_base(LinOp)):
    def apply(self, vector: T) -> T:
        return self.left * (self.right * vector)


class _CompositionAxisOp(_make_composition_op_base(AxisLinOp)):
    def apply_to_axis(self, x: T, axis: int) -> T:
        return self.left.apply_to_axis(self.right.apply_to_axis(x, axis), axis)


def _make_sum_op_base(base_cls):
    @util.dataclass()
    class _SumOpBase(base_cls):
        """A linear operator for the sum of other operators."""
        summands: t.Tuple[base_cls, ...]

        def __add__(self, other: base_cls) -> base_cls:
            if isinstance(other, base_cls):
                return self.__class__(self.summands + (other, ))
            if isinstance(other, self.__class__):
                return self.__class__(self.summands + other.summands)
            return NotImplemented

        def __radd__(self, other: base_cls) -> base_cls:
            if isinstance(other, base_cls):
                return self.__class__((other, ) + self.summands)
            if isinstance(other, self.__class__):
                return self.__class__(other.summands + self.summands)
            return NotImplemented

        @property
        def shape(self) -> Shape:
            return _sum_shape(self.summands)

        @property
        def dtype(self) -> tf.DType:
            if len(set(s.dtype for s in self.summands)) != 1:
                raise ValueError(
                    f'All summand operators must have the same dtype: '
                    f'{self.summands}')
            return self.summands[0].dtype

    return _SumOpBase


class SumOp(_make_sum_op_base(LinOp)):
    def apply(self, vector: T) -> T:
        values = [summand * vector for summand in self.summands]
        return tf.add_n(values)


class SumAxisOp(_make_sum_op_base(AxisLinOp)):
    def apply_to_axis(self, x, axis):
        return tf.add_n(
            [summand.apply_to_axis(x, axis) for summand in self.summands])


@util.dataclass(repr=False)
class DenseMatrixOp(AxisLinOp):
    """Linear operator implemented as a dense matrix."""
    matrix: T

    def apply_to_axis(self, x: T, axis) -> T:
        axes = [[axis], [1]]
        result = tf.tensordot(x, self.matrix, axes)
        return util.ensure_dtype(result, x.dtype)

    @property
    def shape(self) -> Shape:
        return tf.shape(self.matrix)

    @property
    def dtype(self) -> tf.DType:
        return self.matrix.dtype


@util.dataclass(repr=False)
class ScalarOp(AxisLinOp):
    value: T

    def apply_to_axis(self, x, axis) -> T:
        return self.value * x

    @property
    def shape(self) -> Shape:
        return _SquareShape

    @property
    def dtype(self) -> tf.DType:
        return self.value.dtype


@util.dataclass()
class TensorProductOp(LinOp):
    """A linear operator for the tensor product of other operators."""
    sub_ops: t.Tuple[AxisLinOp, ...]

    def apply(self, vector):
        y = tf.reshape(vector, [sub_op.shape[1] for sub_op in self.sub_ops])
        for axis, sub_op in enumerate(self.sub_ops):
            y = sub_op.apply_to_axis(y, axis=axis)
        return tf.reshape(y, (-1, ))

    @property
    def shape(self) -> Shape:
        return (
            tf.reduce_prod([sub_op.shape[0] for sub_op in self.sub_ops]),
            tf.reduce_prod([sub_op.shape[1] for sub_op in self.sub_ops]),
        )

    @property
    def dtype(self) -> tf.DType:
        if len(set(sub_op.dtype for sub_op in self.sub_ops)) != 1:
            raise ValueError(
                f'All tensor product operators must have the same dtype: '
                f'{self.sub_ops}')
        return self.sub_ops[0].dtype

    # def log_det(self) -> T:
    #     # Given a tensor product $A = \bigotimes_i A_i$ of matrices $A_i$, the
    #     # log determinant $L$ is given by
    #     #     $L = R \sum_i L_i / R_i,
    #     # where $L_i = \log\det A_i$, $R_i = \rank A_i$, and $R = \prod_i R_i$.
    #     # See e.g. Petersen, Kaare Brandt, and Michael Syskind Pedersen. "The
    #     # matrix cookbook" (2008).
    #     ranks = (sub_op.shape[0] for sub_op in self.sub_ops)
    #     R = _prod(ranks)
    #     log_dets = (sub_op.log_det() for sub_op in self.sub_ops)
    #     return R * _prod(
    #         log_det / rank for log_det, rank in zip(log_dets, ranks))


@util.dataclass()
class GridInterpOp(LinOp):
    left_grid_interp: interp.GridInterp
    lin_op: LinOp
    right_grid_interp: interp.GridInterp

    def apply(self, vector: T) -> T:
        W_v = self.right_grid_interp.adjoint_to_grid(vector)
        M_W_v = self.lin_op * W_v
        W_M_W_v = self.left_grid_interp.interp_from_grid(M_W_v)
        return W_M_W_v

    @property
    def shape(self) -> Shape:
        return tf.stack(
            [
                self.left_grid_interp.output_point_count,
                self.right_grid_interp.output_point_count
            ],
            axis=0)

    @property
    def dtype(self) -> tf.DType:
        if len({self.left_grid_interp.dtype, self.lin_op.dtype,
                self.right_grid_interp.dtype}) != 1:
            raise ValueError(
                f'Interpolations must have same dtype as operator: '
                f'{self.left_grid_interp}, {self.right_grid_interp}, '
                f'{self.lin_op}')
        return self.lin_op.dtype


@util.dataclass(repr=False)
class ValidSymmConvOp(AxisLinOp):
    """Perform a "valid" convolution along an axis.

    "Valid" means the convolution equivalent to sweeping `kernel` all the way
    across the array axis in consideration without zero padding.

    Note that this is not a general "valid" convolution operator: it can only be
    applied to axes of length `(len(kernel) + 1) // 2`.
    """
    kernel: T
    use_fft: bool = True

    def apply_to_axis(self, x, axis) -> T:
        return util.convolve_1d_valid(
            x,
            util.ensure_dtype(self.kernel, x.dtype),
            use_fft=self.use_fft,
            axis=axis)

    @property
    def shape(self) -> Shape:
        return tf.tile((tf.shape(self.kernel) + 1) // 2, [2])

    @property
    def dtype(self) -> tf.DType:
        return self.kernel.dtype


def simplify(lin_op: LinOp) -> LinOp:
    """Attempt to simplify a `LinOp`."""
    if isinstance(lin_op, (SumOp, SumAxisOp)):
        return _simplify_sum(lin_op)
    if isinstance(lin_op, TensorProductOp):
        return TensorProductOp(
            tuple(simplify(sub_op) for sub_op in lin_op.sub_ops))
    return lin_op


def _simplify_sum(lin_op: t.Union[SumOp, SumAxisOp]) -> LinOp:
    """Attempt to simplify a `Sum[Axis]Op`.

    Merge sums of scalars into a single scalar. Similarly for convolutions and
    dense matrices.
    """
    summands = []
    for s in lin_op.summands:
        if isinstance(s, type(lin_op)):
            summands.extend([simplify(s_) for s_ in s.summands])
        else:
            summands.append(simplify(s))

    def merge(
            summands: t.List[LinOp],
            op_cls: type,
            value_getter: t.Callable[[LinOp], T],
    ) -> t.Tuple[t.Optional[T], t.List[LinOp]]:
        new_summands = [s for s in summands if not isinstance(s, op_cls)]
        values = [value_getter(s) for s in summands if isinstance(s, op_cls)]
        if values:
            return op_cls(tf.add_n(values)), new_summands
        return None, new_summands

    scalar, summands = merge(
        summands,
        ScalarOp,
        lambda lin_op: lin_op.value,
    )
    conv, summands = merge(
        summands,
        ValidSymmConvOp,
        lambda lin_op: lin_op.kernel,
    )
    matrix, summands = merge(
        summands,
        DenseMatrixOp,
        lambda lin_op: lin_op.matrix,
    )

    for term in [scalar, conv, matrix]:
        if term is not None:
            summands.append(term)
    assert summands
    if len(summands) == 1:
        return summands[0]
    return type(lin_op)(summands)


def hadamard_product(dense_matrices: t.List[DenseMatrixOp]) -> DenseMatrixOp:
    """Compute the Hadamard (i.e. elementwise) product of `DenseMatrix`s."""
    hadamard = _prod(dense_matrix.matrix for dense_matrix in dense_matrices)
    return DenseMatrixOp(hadamard)


def _prod(tensors: t.Iterable[T]):
    tensors = list(tensors)
    prod = tensors[0]
    for tensor in tensors[1:]:
        prod *= tensor
    return prod


def _sum_shape(ops: t.Iterable[LinOp]) -> T:
    """Determine shape of a sum of `LinOp`s."""
    for op in ops:
        shape = op.shape
        if shape is not _SquareShape:
            return shape
    raise ValueError(
        f'At least one summand must have an explicit shape: {ops}')
