"""Linear algebra utilities."""

import collections
import enum
import functools
import string
import typing as t

import numpy as np
import tensorflow as tf
import tensorflow.contrib as tfcontrib

from . import linops as l
from . import util

T = tf.Tensor

_AnyDict = t.Dict[str, t.Any]
IntoDetParams = t.Optional[t.Union['DetParams', _AnyDict]]
IntoSolveParams = t.Optional[t.Union['SolveParams', _AnyDict]]
IntoStochTraceParams = t.Optional[t.Union['StochTraceParams', _AnyDict]]
IntoLanczosParams = t.Optional[t.Union['LanczosParams', _AnyDict]]


def solve(A: l.LinOp, b: T, params: IntoSolveParams) -> T:
    """Solve a positive definite linear system of the form Ax = b for x.

    Uses the conjugate gradient method, which only involves matrix-vector
    products.

    TODO: Support preconditioning.
    """
    with tf.name_scope('LinearSolve'):
        params = SolveParams.make(params)
        A = _ConjGradLinOpWrapper(A, params.jitter)

        if params.verbose:
            b = util.print_with_pass_through(b, 'Performing linear solve...')
        sol = tfcontrib.solvers.linear_equations.conjugate_gradient(
            A, b, max_iter=params.max_iter, tol=params.tol)
        x = sol.x
        x.set_shape(b.get_shape())

        converge_success = tf.less(sol.i, params.max_iter)
        r_norm = tf.linalg.norm(sol.r)
        rms_resid = r_norm / tf.sqrt(tf.cast(tf.shape(sol.r)[0], sol.r.dtype))
        reduc_fac = r_norm / tf.linalg.norm(b)

        if params.verbose:
            x = util.print_with_pass_through(x, 'Finished CG solve:')
            x = util.print_with_pass_through(x, '  Steps: ', sol.i)
            x = util.print_with_pass_through(x, '  RMS residual: ', rms_resid)
            x = util.print_with_pass_through(
                x, '  Residual reduction factor: ', reduc_fac)
            x = tf.cond(
                converge_success, lambda: x,
                lambda: util.print_with_pass_through(
                    x, '    -> Convergence failure!'))
        if params.enforce_tol:
            with tf.control_dependencies([tf.Assert(converge_success, [
                    'Convergence failure after',
                    sol.i,
                    'steps, residual reduc. fac.:',
                    reduc_fac,
            ])]):
                x = tf.identity(x)
        if not (params.verbose or params.enforce_tol or params.quiet):
            x = tf.cond(
                converge_success, lambda: x,
                functools.partial(
                    util.print_with_pass_through,
                    x,
                    'Convergence failure in linear solve (linalg.py)',
                    output_stream=tf.logging.warning))

        return x


@util.dataclass()
class _ConjGradLinOpWrapper:
    """Wrapper to handle shape requirements of conjugate gradient solver."""
    lin_op: l.LinOp
    jitter: float

    def apply(self, x: T) -> T:
        shape = x.get_shape()
        y = self.lin_op.apply(x[:, 0])[:, tf.newaxis]
        y.set_shape(shape)
        y += self.jitter * x
        return y

    @property
    def shape(self) -> T:
        return self.lin_op.shape


def inv_quad_form(A: l.LinOp, x: T, Ainv_x: T) -> T:
    """Supply associated analytic gradients for $x^T A^{-1} x$."""
    # We use `tf.stop_gradient` below as a trick to substitute in an analytic
    # gradient rather than running backprop all the way through conjugate
    # gradient (which would be dumb/slow).
    y = tf.stop_gradient(Ainv_x)
    y.set_shape(x.get_shape())
    with tf.name_scope('Forward'):
        forward = _inner_prod(y, x)
    with tf.name_scope('Backward'):
        backward = -_inner_prod(y, A * y) + 2 * _inner_prod(y, x)
    return _supply_analytic_gradient(forward, backward)


def log_det(
        K: l.LinOp,
        params: IntoDetParams = None,
) -> T:
    params = DetParams.make(params)
    method = params.method

    K = l.simplify(K)

    if method == DetMethod.SCALED_EVAL:
        try:
            return log_det_scaled_evals(K)
        except NotImplementedError:
            A = K
            sigma = None
            if isinstance(K, (l.SumOp, l.SumAxisOp)):
                summands = K.summands
                scalars = [s for s in summands if isinstance(s, l.ScalarOp)]
                non_scalars = [
                    s for s in summands if not isinstance(s, l.ScalarOp)
                ]
                if len(non_scalars) == 0:
                    raise ValueError('Cannot take the determinant of a scalar')
                if len(non_scalars) == 1:
                    A = non_scalars[0]
                else:
                    A = type(K)(summands=tuple(non_scalars))
                if len(scalars) == 1:
                    sigma = scalars[0].value
                elif len(scalars) > 1:
                    raise ValueError(
                        f'`simplify` should have combined scalars: {scalars}')
            return log_det_scaled_evals(A, sigma=sigma)
    if method == DetMethod.STOCH_LANCZOS:
        return log_det_stoch_lanczos(
            K,
            solve_params=params.solve_params,
            stoch_trace_params=params.stoch_trace_params,
            lanczos_params=params.lanczos_params)
    raise ValueError(method)


def log_det_stoch_lanczos(
        K: l.LinOp,
        solve_params: IntoSolveParams = None,
        stoch_trace_params: IntoStochTraceParams = None,
        lanczos_params: IntoLanczosParams = None,
) -> T:
    """Estimate the log determinant of a positive definite linear operator.

    Uses stochastic trace estimators together with the Lanczos algorithm. Also
    supplies an analytic gradient.

    See:
        1. Ubaru, Shashanka, Jie Chen, and Yousef Saad. "Fast Estimation of
        tr(f(A)) via Stochastic Lanczos Quadrature." SIAM Journal on Matrix
        Analysis and Applications 38.4 (2017): 1075-1099.
        2. Dong, Kun, et al. "Scalable log determinants for gaussian process
        kernel learning." Advances in Neural Information Processing Systems.
        2017.
    """
    # We use `tf.stop_gradient` below as a trick to substitute in an analytic
    # gradient rather than running backprop all the way through conjugate
    # gradient/Lanczos (which would be dumb/slow).
    with tf.variable_scope('LogDetKrylov'):
        solve_params = SolveParams.make(solve_params)
        stoch_trace_params = StochTraceParams.make(stoch_trace_params)
        lanczos_params = LanczosParams.make(lanczos_params)

        num_probes = stoch_trace_params.num_probes

        Z_shape = (num_probes, K.shape[0])
        Z_init = _make_rademacher_vectors(Z_shape, dtype=K.dtype)
        Z_init /= tf.linalg.norm(Z_init, axis=1, keepdims=True)

        Z = _make_persistent_random_var(Z_init)

        def run_forward_probe(z):
            result = lanczos(K, params=lanczos_params, start_vec=z)
            T_ = result.make_T()
            evals, evecs = tf.linalg.eigh(T_)
            summand = evecs[0]**2 * tf.log(tf.abs(evals))
            return tf.reduce_sum(summand)

        def run_backward_probe(z):
            q = tf.stop_gradient(solve(K, z, solve_params))
            q.set_shape(z.get_shape())
            return _inner_prod(z, K * q)

        norm = tf.cast(num_probes, K.dtype) / tf.cast(K.shape[0], K.dtype)
        with tf.name_scope('Forward'):
            forward = tf.reduce_sum(tf.map_fn(run_forward_probe, Z)) / norm
        with tf.name_scope('Backward'):
            backward = tf.reduce_sum(tf.map_fn(run_backward_probe, Z)) / norm

        return _supply_analytic_gradient(forward, backward)


def log_det_scaled_evals(
        K: l.LinOp,
        sigma: t.Optional[l.ScalarOp] = None,
) -> T:
    r"""Approximation for $\log\det K$ when `K` has efficient eigenvalues."""
    if isinstance(K, l.GridInterpOp):
        n = K.shape[0]
        m = tf.cast(K.lin_op.shape[0], n.dtype)
        scale = tf.cast(n, K.dtype) / tf.cast(m, K.dtype)
        evals = approx_evals(K.lin_op)
        with tf.control_dependencies([tf.assert_greater(
                m, n, message=('Scaled eigenvalue approximation currently '
                               'requires more grid points than points'))]):
            evals = tf.contrib.framework.sort(evals)[::-1][:n]
        evals *= scale
    else:
        evals = approx_evals(K)

    if sigma is not None:
        evals += sigma

    with tf.control_dependencies([tf.assert_positive(
            evals, message='Eigenvalues were not all positive')]):
        return tf.reduce_sum(tf.log(evals))


def approx_evals(K: l.LinOp):
    """Compute approximate eigenvalues of certain `LinOp`s."""
    if isinstance(K, l.DenseMatrixOp):
        return tf.linalg.eigvalsh(K.matrix)
    if isinstance(K, l.ValidSymmConvOp):
        # So long as `K.kernel` decays significantly away from its center, the
        # eigenvalues of `K` are densely distributed in the Fourier series
        # associated with `K.kernel` evaluated on $[0, 2\pi)$. A discrete
        # approximation of this set is the type-III DCT of `K.kernel`, which is
        # what we use here. See Reichel, Lothar, and Lloyd N. Trefethen.
        # "Eigenvalues and pseudo-eigenvalues of Toeplitz matrices." Linear
        # algebra and its applications 162 (1992): 153-185.
        half_kernel = K.kernel[K.shape[0]:]
        if not half_kernel.dtype == tf.float32:
            half_kernel = tf.cast(half_kernel, tf.float32)
        evals = tf.spectral.dct(half_kernel, type=3)
        if not evals.dtype == K.dtype:
            evals = tf.cast(evals, K.dtype)
        return evals
    if isinstance(K, l.TensorProductOp):
        sub_evals = [approx_evals(sub_op) for sub_op in K.sub_ops]
        return _outer_flat(sub_evals)
    raise NotImplementedError(f'No efficient eigenvalue routine for {K}')


@util.dataclass()
class LanczosResult:
    # Number of iteration steps used.
    i: T
    # Orthogonal matrix of Lanczos vectors.
    Q: T
    # Diagonal.
    alpha: T
    # Off-diagonal.
    beta: T

    def make_T(self):
        return _make_band_matrix({0: self.alpha, 1: self.beta, -1: self.beta})


def lanczos(
        lin_op: l.LinOp,
        start_vec: T,
        params: IntoLanczosParams = None,
) -> LanczosResult:
    """Lanczos tridiagonalization algorithm."""
    state_cls = collections.namedtuple(
        'LanczosState', ['i', 'alpha', 'beta', 'r', 'Q', 'order'])

    params = LanczosParams.make(params)

    def loop_continue(*state_args) -> T:
        state = state_cls(*state_args)
        return (
            (state.i + 1 < params.max_order) & tf.equal(state.i, state.order))

    def loop_body(*state_args) -> state_cls:
        state = state_cls(*state_args)

        i_prev = state.i
        beta_prev = state.beta.read(i_prev)
        q_prev = state.Q.read(i_prev)

        i = i_prev + 1
        q = state.r / beta_prev
        Q = state.Q.write(i, q)
        r = lin_op.apply(q) - beta_prev * q_prev
        alpha = _inner_prod(q, r)
        # This computes `alpha` again but whatever.
        r, beta = _orthogonalize(i + 1, Q, r)
        # Attempt to detect invariant subspace.
        order = tf.cond(
            tf.abs(beta) > _get_rtol(beta.dtype) * tf.abs(alpha),
            lambda: i,
            lambda: i_prev,
        )

        return state_cls(
            i=i,
            alpha=state.alpha.write(i, alpha),
            beta=state.beta.write(i, beta),
            r=r,
            Q=Q,
            order=order,
        )

    with tf.name_scope('Lanczos'):
        start_vec = util.ensure_tensor(start_vec, preferred_dtype=tf.float32)
        q_0 = start_vec / tf.linalg.norm(start_vec)
        dtype = q_0.dtype

        def make_tensor_array(name: str, init: T) -> tf.TensorArray:
            return tf.TensorArray(
                size=0,
                dynamic_size=True,
                tensor_array_name=name,
                dtype=dtype,
                clear_after_read=False,
            ).write(0, init)

        r = lin_op.apply(q_0)
        alpha_0 = _inner_prod(q_0, r)
        r -= alpha_0 * q_0
        beta_0 = tf.linalg.norm(r)

        alpha = make_tensor_array('alpha', alpha_0)
        beta = make_tensor_array('beta', beta_0)
        Q = make_tensor_array('Q', q_0)

        init_state = state_cls(i=0, alpha=alpha, beta=beta, Q=Q, r=r, order=0)
        # Attempt to detect invariant subspace.
        final_state = tf.cond(
            tf.abs(beta_0) > _get_rtol(beta_0.dtype) * tf.abs(alpha_0),
            lambda: tf.while_loop(
                cond=loop_continue, body=loop_body, loop_vars=init_state),
            lambda: init_state,
        )

        return LanczosResult(
            i=final_state.i,
            alpha=final_state.alpha.stack(),
            beta=final_state.beta.stack()[:-1],
            Q=tf.transpose(final_state.Q.stack()),
        )


def _orthogonalize(i: T, basis: tf.TensorArray, v: T):
    """Orthogonalize `v` against `basis[:i]`."""

    def gram_schmidt_step(j: T, basis: tf.TensorArray, v: T):
        """Makes `v` orthogonal to the `j`th vector in `basis`."""
        basis_vec = basis.read(j)
        v -= _inner_prod(basis_vec, v) * basis_vec
        return j + 1, basis, v

    def orthogonalize_once(i: T, basis: tf.TensorArray, v: T):
        """Perform actual orthogonalization."""
        _, _, v = tf.while_loop(
            lambda j, basis, v: j < i, gram_schmidt_step,
            [0, basis, v])
        norm = tf.linalg.norm(v)
        return v, norm

    with tf.name_scope('Orthogonalize'):
        v_norm = tf.linalg.norm(v)
        v_new, v_new_norm = orthogonalize_once(i, basis, v)
        # If the norm decreases more than 1/sqrt(2), run a second round of MGS.
        # See proof in:
        #   B. N. Parlett, ``The Symmetric Eigenvalue Problem'',
        #   Prentice-Hall, Englewood Cliffs, NJ, 1980. pp. 105-109
        return tf.cond(
            v_new_norm < 0.7071 * v_norm,
            lambda: orthogonalize_once(i, basis, v_new),
            lambda: (v_new, v_new_norm),
        )


def _inner_prod(vec_1: T, vec_2: T) -> T:
    with tf.name_scope('InnerProd'):
        return tf.reduce_sum(vec_1 * vec_2, axis=-1)


def _make_band_matrix(band_offset_to_vector: t.Mapping[T, T]) -> T:
    """Generate a band-diagonal matrix."""
    with tf.name_scope('MakeBandMatrix'):
        matrices = []
        for offset, vector in band_offset_to_vector.items():
            matrix = tf.diag(vector)

            offset = tf.convert_to_tensor(offset, dtype=tf.int32)
            abs_offset = tf.abs(offset)
            left = [abs_offset, 0]
            right = [0, abs_offset]
            paddings = tf.cond(
                offset > 0, lambda: [right, left], lambda: [left, right])  # pylint: disable=cell-var-from-loop

            matrices.append(tf.pad(matrix, paddings))
        return tf.add_n(matrices)


def _make_persistent_random_var(Z_init: T) -> T:
    """Make a random tensor that will not change between `session.run` calls.

    Defines a `Variable` to do this.

    Args:
        Z_init: A `Tensor` depending on a random number generator, e.g.
            `tf.random_uniform`.

    Returns:
        A `Variable` that will always produce the first incarnation of `Z_init`.
    """
    with tf.variable_scope('PersistentRand'):
        Z_var = tf.get_variable(
            name='Z_var',
            initializer=Z_init,
            trainable=False,
            validate_shape=Z_init.get_shape().is_fully_defined(),
        )
        Z_var.set_shape(Z_init.get_shape())
        return Z_var


def _make_rademacher_vectors(shape: T, dtype: tf.DType):
    """Make Rademacher random vectors.

    I.e. vectors where each entry is +1 or -1 with equal probability.
    """
    with tf.name_scope('RademacherVectors'):
        vectors = tf.random_uniform(
            shape=shape, maxval=2, dtype=tf.int32, seed=1)
        return tf.cast(2 * vectors - 1, dtype)


def _supply_analytic_gradient(forward: T, backward: T) -> T:
    """Helper for supplying analytic gradients.

    Takes two versions of a scalar -- `forward` for directly computing values
    and `backward` for computing gradients -- and splices them into one
    appropriate `Tensor`.

    https://stackoverflow.com/a/36480182/2223436
    """
    return backward + tf.stop_gradient(forward - backward)


class _Params:
    """Base class for parameter containers.

    Just provides a `make` convenience `classmethod` that allows passing in a
    fully constructed `_Params` subclass, `None` (for defaults), or a dict of
    field-value pairs (fewer keystrokes sometimes).

    Nested `_Params` are handled automatically, e.g.:

    ```python
    class Blah(_Params):
        x: int = 1

    class Whatever(_Params):
        blah: Blah = Blah()

    def f(whatever, ...):
        whatever = Whatever.make(whatever)
        ...

    # This works.
    f(whatever=dict(blah=dict(x=7)), ...)
    ```
    """

    @classmethod
    def make(cls, arg: t.Optional[t.Union[dict, '_Params']]):
        if isinstance(arg, cls):
            return arg

        if arg is None:
            kwargs = {}
        elif isinstance(arg, dict):
            kwargs = arg
        else:
            raise TypeError

        new_kwargs = {}
        for key, value in kwargs.items():
            field_type = util.get_dataclass_field_type(cls, key)
            if issubclass(field_type, _Params):
                new_kwargs[key] = field_type.make(value)
            else:
                new_kwargs[key] = value
        return cls(**new_kwargs)


@util.dataclass()
class SolveParams(_Params):
    """Parameters for iterative linear solves."""
    tol: float = 1e-2
    max_iter: int = 100
    enforce_tol: bool = False

    # Add "jitter" of this magnitude to the diagonal of the input linear
    # operator for numerical stability.
    jitter: float = 1e-8

    # Print extra solve info to `stdout`.
    verbose: bool = False
    # Don't even print convergence-failure warnings.
    quiet: bool = False


@util.dataclass()
class LanczosParams(_Params):
    """Parameters for Lanczos decompositions."""
    max_order: int = 10


@util.dataclass()
class StochTraceParams(_Params):
    """Parameters for stochastic trace estimation."""
    # Note: Relative error $\epsilon$ of stochastic trace estimation scales as
    # $\epsilon \sim 3 / \sqrt{n_v}$, where $n_v$ is the number of probe
    # vectors. Thus this default of $n_v = 10$ ensures a relative error of only
    # $\epsilon \sim 1$, meaning only up to an order of magnitude. To achieve a
    # relative error of $\epsilon \sim 10^{-2}$, we need $n_v \sim 10^5$. See
    # Ubaru et al. (2017).
    #
    # Also note that this error formula doesn't include the error from
    # approximating a linear operator with its Lanczos decomposition -- see
    # `LanczosParams`.
    num_probes: int = 10


class DetMethod(enum.Enum):
    STOCH_LANCZOS = enum.auto()
    SCALED_EVAL = enum.auto()


@util.dataclass()
class DetParams(_Params):
    """Parameters for computing log determinants."""
    method: DetMethod = DetMethod.STOCH_LANCZOS

    # For `method = STOCH_LANCZOS`.
    solve_params: SolveParams = SolveParams(jitter=1e-4)
    lanczos_params: LanczosParams = LanczosParams()
    stoch_trace_params: StochTraceParams = StochTraceParams()


def _get_rtol(dtype: tf.DType) -> T:
    return util.ensure_tensor(
        np.finfo(dtype.as_numpy_dtype()).resolution * 2.,  # pylint: disable=no-member
        ensure_dtype_is=dtype)


def _outer_flat(vectors: t.Iterable[T]) -> T:
    chars = string.ascii_lowercase[:len(vectors)]
    return tf.reshape(
        tf.einsum(','.join(chars) + '->' + chars, *vectors), (-1, ))
