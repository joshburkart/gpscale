import numpy as np
import pytest
import tensorflow as tf

from . import gridding
from . import hyper
from . import kernels as kernels_lib
from . import util

DTYPE = tf.float64


def test_stationary_ops(tf_session):
    kernel = kernels_lib.RBF()
    with tf.variable_scope('Plain'):
        _test_stationary_kernel_impl(kernel, tf_session)

    kernel = kernels_lib.RBF(length_scale=hyper.positive(2.))
    with tf.variable_scope('LengthScaled'):
        _test_stationary_kernel_impl(kernel, tf_session)

    kernel = kernels_lib.Scalar(7.) * kernels_lib.RBF()
    with tf.variable_scope('VarianceScaled'):
        _test_stationary_kernel_impl(kernel, tf_session)

    kernel = kernels_lib.RBF() + kernels_lib.RBF() + kernels_lib.Gaussian()
    assert isinstance(kernel, kernels_lib.Stationary)
    with tf.variable_scope('Sum'):
        _test_stationary_kernel_impl(kernel, tf_session)

    kernel = kernels_lib.RBF() * kernels_lib.RBF() * kernels_lib.Gaussian()
    assert isinstance(kernel, kernels_lib.Stationary)
    with tf.variable_scope('Product'):
        _test_stationary_kernel_impl(kernel, tf_session)


def test_spectral_mixture(tf_session):
    kernel = kernels_lib.SpectralMixture.make(
        num_components=3, init_min_length_scale=1., init_max_length_scale=10.)
    _test_stationary_kernel_impl(kernel, tf_session)


def test_periodic(tf_session):
    kernel = kernels_lib.Periodic()
    _test_stationary_kernel_impl(kernel, tf_session)


def _test_stationary_kernel_impl(
        kernel: kernels_lib.Stationary, tf_session: tf.Session):
    np.random.seed(1)
    _assert_positive_def(kernel, tf_session)
    _assert_grid_lin_op_consistent(kernel, tf_session)


def _assert_positive_def(
        kernel: kernels_lib.Stationary, tf_session: tf.Session):
    with tf.variable_scope('AssertPositiveDef'):
        buncha_random_points = util.ensure_tensor(np.random.normal(size=100))
        kernel_matrix_tens = kernel.make_lin_op(buncha_random_points).matrix

        kernel_matrix = tf_session.run(kernel_matrix_tens)

        evals = np.linalg.eigvals(kernel_matrix)
        np.testing.assert_allclose(evals.imag, 0., atol=1e-4)
        np.testing.assert_array_less(-3e-5, evals.real)


def _assert_grid_lin_op_consistent(
        kernel: kernels_lib.Stationary, tf_session: tf.Session):
    with tf.variable_scope('AssertGridLinOpConsistent', reuse=tf.AUTO_REUSE):
        linspace_args = [-np.pi, 17., 7]
        grid = gridding.Grid1D(*linspace_args)
        grid_points = np.linspace(*linspace_args)
        grid_values = util.ensure_tensor(
            np.random.uniform(size=linspace_args[2]))

        with tf.variable_scope('Dense'):
            dense_lin_op = kernel.make_lin_op(util.ensure_tensor(grid_points))
        with tf.variable_scope('Grid'):
            grid_lin_op = kernel.make_grid_lin_op(
                grid, dtype=grid_values.dtype)

        dense_matvec = tf_session.run(dense_lin_op * grid_values)
        grid_matvec = tf_session.run(grid_lin_op * grid_values)

        np.testing.assert_allclose(dense_matvec, grid_matvec, rtol=1e-4)


def test_tensor_product_kernel(tf_session):
    """Test both a `TensorProduct` kernel and a sum of two."""
    x1 = 1.2
    x2 = 2.5
    X_in_np = [[0., 0.], [x1, x2]]
    Y_in_np = [1., 0.]
    subkernel = kernels_lib.RBF(length_scale=1.)
    kernel = kernels_lib.TensorProduct([subkernel, subkernel])
    kernels = [kernel, kernel + kernel]
    expected_val = tf_session.run(
        subkernel.kernel_func(x1**2) * subkernel.kernel_func(x2**2))
    expected_vals = [expected_val, 2 * expected_val]

    for kernel, expected_val in zip(kernels, expected_vals):
        K = kernel.make_lin_op(util.ensure_tensor(X_in_np, name='X_in'))
        val = tf_session.run((K * util.ensure_tensor(Y_in_np))[1])
        np.testing.assert_allclose(val, expected_val, rtol=1e-4)


def test_grid_interp_tensor_product_kernel(tf_session):
    """Test both a `GridInterpTensorProduct` kernel and a sum of two."""
    x1 = 1.2
    x2 = 3.1
    X_in_np = [[0., 0.], [x1, x2]]
    Y_in_np = [1., 0.]
    subkernel = kernels_lib.Scalar() * kernels_lib.RBF()
    grid = gridding.Grid1D(0., 10., 101)
    product_grid = gridding.ProductGrid((grid, ) * 2)
    kernel = kernels_lib.TensorProduct((subkernel, ) * 2)
    grid_kernels = [
        kernel.make_grid_interp_kernel(product_grid),
        (kernel + kernel).make_grid_interp_kernel(product_grid),
    ]
    with tf.variable_scope('Scope', reuse=tf.AUTO_REUSE):
        expected_val = tf_session.run(
            subkernel.kernel_func(x1**2) * subkernel.kernel_func(x2**2))
    expected_vals = [expected_val, 2 * expected_val]

    for i, (grid_kernel, expected_val) in enumerate(zip(grid_kernels,
                                                        expected_vals)):
        with tf.variable_scope(str(i), reuse=tf.AUTO_REUSE):
            K = grid_kernel.make_lin_op(
                util.ensure_tensor(X_in_np, name='X_in'))
            val = tf_session.run((K * util.ensure_tensor(Y_in_np))[1])

        np.testing.assert_allclose(val, expected_val, rtol=1e-3)


if __name__ == '__main__':
    pytest.main()
