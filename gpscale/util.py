"""Global utilities."""

import contextlib
import typing as t

import attr
import numpy as np
import tensorflow as tf

IntoTensor = t.Union[tf.Tensor, tf.Variable, np.ndarray, t.List]
T = tf.Tensor


def ensure_tensor(
        arg: IntoTensor,
        name: str = None,
        preferred_dtype: tf.DType = tf.float32,
        ensure_rank_is: int = None,
        ensure_dtype_is: t.Optional[tf.DType] = None,
) -> tf.Tensor:
    """Convert `arg` to a `Tensor` if it isn't already one."""
    tensor = tf.convert_to_tensor(
        arg, name=name, preferred_dtype=ensure_dtype_is or preferred_dtype)

    if ensure_rank_is is not None:
        tensor = ensure_rank(tensor, ensure_rank_is)

    if ensure_dtype_is is not None:
        tensor = ensure_dtype(tensor, ensure_dtype_is)

    return tensor


def ensure_rank(tensor: T, rank: int) -> T:
    input_rank = get_rank(tensor)
    if input_rank < rank:
        reshaper = (..., ) + (tf.newaxis, ) * (rank - input_rank)
        tensor = tensor[reshaper]
    elif input_rank > rank:
        num_extra_dims = input_rank - rank
        if tuple(tensor.get_shape())[rank:] == (1, ) * num_extra_dims:
            reshaper = (..., ) + (0, ) * num_extra_dims
            tensor = tensor[reshaper]
        else:
            raise ValueError(
                f'For input tensor {tensor}, expected {rank} axes, but '
                f'received {input_rank}')
    return tensor


def ensure_dtype(tensor, dtype: tf.DType):
    if tensor.dtype != dtype:
        return tf.cast(tensor, dtype)
    return tensor


def get_rank(tensor: T) -> int:
    return len(tensor.get_shape())


def get_shape(tensor: T):
    return tuple(tensor.get_shape())


def print_with_pass_through(pass_through: T, *args, **kwargs):
    with tf.control_dependencies([tf.print(*args, **kwargs)]):
        return tf.identity(pass_through)


@contextlib.contextmanager
def print_multi(*arg_lists):
    with contextlib.ExitStack() as stack:
        for arg_list in arg_lists:
            stack.enter_context(tf.control_dependencies([tf.print(*arg_list)]))
        yield


def convolve_1d_valid(
        array: T,
        kernel: T,
        axis: int = 0,
        use_fft: bool = True,
        scope=None,
):
    """Convolve an N-D array with a 1-D kernel.

    "Valid" means the convolution equivalent to sweeping `kernel` all the way
    across `array` (or the reverse) without zero padding.

    Non-obvious args:
        axis: Which axis of `array` to convolve `kernel` with.
        use_fft: A bool indicating whether to convolve using FFTs.
    """
    with tf.name_scope(scope, default_name='Convolve1DValid'):
        if use_fft:
            return _convolve_1d_valid_fft(array, kernel, axis=axis)
        return _convolve_1d_valid_direct(array, kernel, axis=axis)


def _convolve_1d_valid_direct(array: T, kernel: T, axis: int = 0):
    """Convolve an N-D array with a 1-D kernel using direct sums.

    This function should be as simple as a single call, but unfortunately
    `tf.nn.convolution` was designed by neural network meatheads :P,
    necessitating jumping through various reshaping hoops.
    """
    with tf.name_scope('Direct'):
        array_len = tf.shape(array)[axis]
        kernel_len = tf.shape(kernel)[0]

        # We may need to zero pad `array` since "valid" padding here requires
        # `array_len` >= `kernel_len`. We'll trim the resulting convolution
        # later.
        def zero_pad():
            paddings = [[0, 0]] * get_rank(array)
            paddings[axis] = [kernel_len - array_len] * 2
            return tf.pad(array, paddings)

        array = tf.cond(array_len >= kernel_len, lambda: array, zero_pad)

        # Add fake axes to `kernel` so that it has the same rank as `array`.
        vector_shape = [1] * get_rank(array)
        vector_shape[axis] = tf.shape(kernel)[0]
        kernel = tf.reshape(kernel, tuple(vector_shape))

        # Append fake batch and channel axes.
        array = array[tf.newaxis, ..., tf.newaxis]

        # Add fake channel axes.
        kernel = kernel[..., tf.newaxis, tf.newaxis]

        # Last idiotic thing: `tf.nn.convolution` actually computes a
        # cross-correlation, not a convolution. Thus we need to reverse
        # `kernel`.
        kernel = tf.reverse(kernel, axis=[axis])

        conv_padded = tf.nn.convolution(array, kernel, 'VALID')

        # Remove fake batch and channel axes.
        return conv_padded[0, ..., 0]


def _convolve_1d_valid_fft(array: T, kernel: T, axis: int = 0):
    """Convolve an N-D array with a 1-D kernel using FFTs."""
    with tf.name_scope('FFT'):
        array_len = tf.shape(array)[axis]
        kernel_len = tf.shape(kernel)[0]

        fft_len = array_len + kernel_len - 1

        array_rfft = _rfft(array, fft_len=fft_len, axis=axis)
        vector_rfft = _rfft(kernel, fft_len=fft_len, axis=0)

        array_rank = get_rank(array)
        if array_rank != 1:
            new_shape = [1] * array_rank
            new_shape[axis] = -1
            conv_rfft = array_rfft * tf.reshape(vector_rfft, new_shape)
        else:
            conv_rfft = array_rfft * vector_rfft
        conv_full = tf.real(_irfft(conv_rfft, fft_len=fft_len, axis=axis))
        conv = _make_valid_conv(
            output_conv_len=tf.maximum(array_len, kernel_len) - tf.minimum(
                array_len, kernel_len) + 1,
            conv=conv_full,
            axis=axis)

        return ensure_dtype(conv, array.dtype)


def searchsorted(sorted_array: T, values_to_insert: T, scope=None, dtype=None):
    """Determine indices within a sorted array at which to insert elements.

    Equivalent of `np.searchsorted`.

    Currently only 1D arrays are supported, but should be easy to extend.
    """
    dtype = dtype or tf.int64

    cast = lambda tensor: ensure_dtype(tensor, dtype)

    with tf.name_scope(scope, default_name='SearchSorted'):
        sorted_array_shape = cast(tf.shape(sorted_array))
        values_shape = cast(tf.shape(values_to_insert))

        sorted_array_len = sorted_array_shape[0]
        values_len = values_shape[0]

        # Put everything together and argsort.
        concat = tf.concat([sorted_array, values_to_insert], axis=0)
        concat_argsort = tf.contrib.framework.argsort(concat, stable=True)
        concat_argsort = cast(concat_argsort)

        # Find indices into `sorted_array` where values should be inserted (but
        # not yet in the right order).
        insert_mask = concat_argsort >= sorted_array_len
        sorted_insert_indices = tf.reshape(
            cast(tf.where(insert_mask)), values_shape) - tf.range(
                cast(values_len))

        # Construct permutation specifying ordering of insertion indices.
        insert_perm = tf.boolean_mask(
            concat_argsort, insert_mask) - sorted_array_len
        # The permutation we created satisfies, in NumPy notation,
        # `sorted_insert_indices = insert_indices[insert_perm]`. With TF this is
        # instead `sorted_insert_indices = tf.gather(insert_indices,
        # insert_perm)`. But we want `insert_indices`, so we need the inverse,
        # which is `tf.scatter_nd`.
        insert_indices = tf.scatter_nd(
            cast(insert_perm)[..., tf.newaxis],
            sorted_insert_indices,
            shape=cast(values_shape))
    return insert_indices


def dataclass(frozen=True, **kwargs):
    return attr.dataclass(frozen=frozen, **kwargs)


def get_dataclass_field_type(cls: type, field_name: str):
    return attr.fields_dict(cls)[field_name].type


def _rfft(x: T, fft_len: T, axis: int, scope=None):
    return _fft_impl(
        x=ensure_dtype(x, tf.float32),
        fft_len=fft_len,
        axis=axis,
        scope=scope,
        default_name='RFFT',
        fft_func=tf.spectral.rfft)


def _irfft(x: T, fft_len: T, axis: int, scope=None):
    return _fft_impl(
        x=ensure_dtype(x, tf.complex64),
        fft_len=fft_len,
        axis=axis,
        scope=scope,
        default_name='IRFFT',
        fft_func=tf.spectral.irfft)


def _fft_impl(
        x: T,
        fft_len: int,
        axis: int,
        scope: t.Optional[str],
        default_name: str,
        fft_func: t.Callable,
):
    with tf.name_scope(scope, default_name=default_name):
        # `tf.spectral.[i]rfft` take FFTs over the "innermost" -- i.e., last --
        # axis.
        fft_axis = get_rank(x) - 1

        if axis != fft_axis:
            x = _transpose(x, fft_axis, axis)

        fft = fft_func(x, fft_length=[fft_len])

        if axis != fft_axis:
            fft = _transpose(fft, fft_axis, axis)

        return fft


def _transpose(x: T, axis_1: int, axis_2: int):
    perm = list(range(len(x.get_shape())))
    perm[axis_1] = axis_2
    perm[axis_2] = axis_1
    return tf.transpose(x, perm)


def _make_valid_conv(output_conv_len, conv, axis):
    """Convert a larger convolution into a 'valid' padded convolution."""
    input_len = tf.shape(conv)[axis]
    left_offset_len = (input_len - output_conv_len) // 2
    output_slices = (
        (slice(None), ) * axis +
        (slice(left_offset_len, left_offset_len + output_conv_len), ))
    return conv[output_slices]
