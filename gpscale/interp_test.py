import numpy as np
import pytest
import tensorflow as tf

from . import interp


def test_1d_interp_random_ordinate(tf_session):
    """Test with random values."""
    np.random.seed(1)

    from_points = np.arange(10, dtype=np.float32)
    to_points = [-1., 0., 5., 9., 10., 6.5]
    grid_interp = interp.make_1d_linear_grid_interp(
        from_points=from_points, to_points=to_points)

    from_values = np.random.uniform(0., 1., len(from_points))
    to_values = tf_session.run(grid_interp.interp_from_grid(from_values))

    expected_to_values = np.array(
        [
            from_values[0],
            from_values[0],
            from_values[5],
            from_values[-1],
            from_values[-1],
            (from_values[6] + from_values[7]) / 2,
        ])
    np.testing.assert_allclose(to_values, expected_to_values)


def test_1d_interp_line(tf_session):
    """Test interpolating a line."""
    from_points = np.arange(10, dtype=np.float32)
    to_points = np.linspace(-5, 50, 1000, dtype=np.float32)
    grid_interp = interp.make_1d_linear_grid_interp(
        from_points=from_points, to_points=to_points)

    from_values = from_points
    to_values = tf_session.run(grid_interp.interp_from_grid(from_values))

    left_extrap_mask = to_points < from_points[0]
    right_extrap_mask = to_points > from_points[-1]
    interp_mask = ~(left_extrap_mask | right_extrap_mask)

    np.testing.assert_allclose(to_values[interp_mask], to_points[interp_mask])
    np.testing.assert_allclose(to_values[left_extrap_mask], from_points[0])
    np.testing.assert_allclose(
        to_values[right_extrap_mask], from_points[-1], rtol=1e-4)


def test_grid_interp_matrix(tf_session):
    """Test the internals of `GridInterp` with 1D inputs."""
    grid_inds_by_row = tf.convert_to_tensor([
        [1, 2],
        [0, 1],
        [1, 3],
    ])[:, :, tf.newaxis]
    weights_by_row = tf.reshape(
        tf.range(6, dtype=tf.float32), grid_inds_by_row.shape[:2])
    grid_point_counts = tf.convert_to_tensor([5])

    grid_interp = interp.GridInterp.make(
        grid_inds_by_row=grid_inds_by_row,
        weights_by_row=weights_by_row,
        grid_point_counts=grid_point_counts)

    interp_tensor, interp_matrix = tf_session.run(
        [
            tf.sparse.to_dense(grid_interp.interp_tensor),
            tf.sparse.to_dense(grid_interp.interp_matrix),
        ])

    expected_interp_tensor = np.array(
        [
            [0., 0., 1., 0., 0.],
            [2., 3., 0., 0., 0.],
            [0., 4., 0., 5., 0.],
        ])
    np.testing.assert_allclose(interp_tensor, expected_interp_tensor)
    np.testing.assert_allclose(interp_matrix, expected_interp_tensor)


def test_grid_interp_tensor(tf_session):
    """Test the internals of `GridInterp` with 2D inputs."""
    grid_inds_by_row = tf.convert_to_tensor([[[0, 1], [1, 2]]])
    weights_by_row = tf.convert_to_tensor([[5., 7.]])
    grid_point_counts = tf.convert_to_tensor([2, 3])

    grid_interp = interp.GridInterp.make(
        grid_inds_by_row=grid_inds_by_row,
        weights_by_row=weights_by_row,
        grid_point_counts=grid_point_counts)

    interp_tensor, interp_matrix = tf_session.run(
        [
            tf.sparse.to_dense(grid_interp.interp_tensor),
            tf.sparse.to_dense(grid_interp.interp_matrix),
        ])

    expected_interp_tensor = np.array([[[0., 5., 0.], [0., 0., 7.]]])
    expected_interp_matrix = expected_interp_tensor.reshape(((1, 6)))
    np.testing.assert_allclose(interp_tensor, expected_interp_tensor)
    np.testing.assert_allclose(interp_matrix, expected_interp_matrix)


if __name__ == '__main__':
    pytest.main()
