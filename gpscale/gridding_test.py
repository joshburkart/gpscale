import numpy as np
import pytest
import scipy.interpolate
import tensorflow as tf

from . import gridding
from . import util

_EPSILON = 1e-6


def test_interp_from_grid_1d(tf_session):
    """Put some values on a 1D grid, then interpolate to other points."""
    linspace_args = [0., 10., 11]
    grid = gridding.Grid1D(*linspace_args)
    grid_points_array = np.linspace(*linspace_args)

    grid_values = np.zeros_like(grid_points_array)
    grid_values[1] = 1.
    grid_values[3] = 77.
    grid_values[5] = 10.
    grid_values[9] = 100.

    points = [1.001, 3., 5.5, 8.25]
    grid_interp = grid.make_grid_interp(util.ensure_tensor(points))

    interp_values = grid_interp.interp_from_grid(grid_values)
    np.testing.assert_allclose(
        tf_session.run(interp_values), [0.999, 77., 5, 25])


def test_interp_from_grid_2d(tf_session):
    """Put some values on a 2D grid, then interpolate to other points."""
    min_1 = 0.
    max_1 = 1.
    min_2 = 1.
    max_2 = 3.
    point_count_1 = 2
    point_count_2 = 2
    grid_1 = gridding.Grid1D(min_1, max_1, point_count_1)
    grid_2 = gridding.Grid1D(min_2, max_2, point_count_2)
    grid_values = np.random.uniform(size=(point_count_1, point_count_2))

    prod_grid = gridding.ProductGrid([grid_1, grid_2])

    points = [
        [min_1 + _EPSILON, min_2 + _EPSILON],
        [min_1 + _EPSILON, max_2 - _EPSILON],
        [max_1 - _EPSILON, min_2 + _EPSILON],
        [max_1 - _EPSILON, max_2 - _EPSILON],
        [(min_1 + max_1) / 2,
         (min_2 + max_2) / 2],
    ]

    grid_interp = prod_grid.make_grid_interp(util.ensure_tensor(points))
    interp_tensor = tf_session.run(
        tf.sparse.to_dense(grid_interp.interp_tensor))
    interp_values = np.einsum('ijk,jk', interp_tensor, grid_values)
    np.testing.assert_allclose(
        interp_values, [
            grid_values[0, 0],
            grid_values[0, 1],
            grid_values[1, 0],
            grid_values[1, 1],
            grid_values.mean(),
        ],
        rtol=5e-4)


def test_adjoint_to_grid(tf_session):
    """Test interpolation adjoint."""
    max_val = 4.
    point_count = int(max_val) + 1
    grid = gridding.Grid1D(0., max_val, point_count)

    points = [3.5]
    values = np.ones_like(points)
    grid_interp = grid.make_grid_interp(util.ensure_tensor(points))

    grid_values = grid_interp.adjoint_to_grid(
        util.ensure_tensor(values, ensure_rank_is=2))
    expected_grid_values = np.zeros(point_count)
    expected_grid_values[[3, 4]] = 0.5
    np.testing.assert_allclose(
        tf_session.run(grid_values), expected_grid_values)


def test_interp_from_grid_2d_vs_scipy(tf_session):
    linspace_args = [0., 1., 2]
    d = 2
    grid_points = np.linspace(*linspace_args)
    grid_values = np.arange(2**d).reshape((2, ) * d)
    interp = scipy.interpolate.RectBivariateSpline(
        grid_points, grid_points, grid_values, kx=1, ky=1)

    grid = gridding.Grid1D(*linspace_args)
    prod_grid = gridding.ProductGrid((grid, ) * d)

    interp_points = np.random.uniform(size=(100, 2))
    grid_interp = prod_grid.make_grid_interp(
        util.ensure_tensor(interp_points, preferred_dtype=tf.float32))

    interp_values = grid_interp.interp_from_grid(
        util.ensure_tensor(grid_values, preferred_dtype=tf.float32))

    np.testing.assert_allclose(
        tf_session.run(interp_values),
        interp(interp_points[:, 0], interp_points[:, 1], grid=False),
        rtol=1e-5)


if __name__ == '__main__':
    pytest.main()
