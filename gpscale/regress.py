"""Code for Gaussian process regression.

TODO: Simplify using `tf.Template`s:
https://www.tensorflow.org/api_docs/python/tf/make_template.
"""

import abc
import collections
import typing as t

import numpy as np
import tensorflow as tf

from . import kernels
from . import linalg
from . import linops
from . import util

T = tf.Tensor


class MeanFunc(metaclass=abc.ABCMeta):
    def mean_func(self, X: T) -> T:
        with tf.variable_scope(self.__class__.__name__):
            return self._mean_func(X)

    @abc.abstractmethod
    def _mean_func(self, X: T) -> T:
        pass


@util.dataclass()
class ConstantMean(MeanFunc):
    """A (learnable) constant mean function."""
    value: T = 0.

    def _mean_func(self, X: T) -> T:
        return tf.get_variable(
            name='mean',
            initializer=util.ensure_tensor(
                self.value, ensure_rank_is=1, ensure_dtype_is=X.dtype),
            dtype=X.dtype,
        )


class LossComponents(collections.namedtuple(
        'LossComponents', ['log_likelihood', 'data_fit', 'complexity'])):
    """Container for computed losses.

    `log_likelihood` is what should be optimized, but sometimes useful to see
    the values of its constituent terms.
    """


@util.dataclass(repr=False)
class Model:
    """GP regression model.

    Notation:
        Prefixes:
            X_*: Data from the "index set", I.e. independent variable data.
            f_*: Data from the "value set". I.e. dependent variables data.
            Y_*: Data from the "value set" but contaminated with noise.
        Suffixes:
            *_in: Input data.
            *_out: Output data, e.g. interpolation locations.
    """
    # Inputs.
    X_in: tf.Tensor
    Y_in: tf.Tensor

    X_out: t.Optional[tf.Tensor]

    kernel: kernels.Kernel
    noise_kernel: kernels.Kernel

    mean_func: MeanFunc

    solve_params: linalg.SolveParams
    det_params: linalg.DetParams

    dtype: tf.DType

    # Outputs.
    f_out_mean: t.Optional[tf.Tensor]
    # TODO: Add posterior variance estimates.
    losses: LossComponents

    @classmethod
    def build(  # pylint: disable=too-many-locals
            cls,
            *,
            X_in: util.IntoTensor,
            Y_in: util.IntoTensor,
            kernel: kernels.Kernel,
            X_out: t.Optional[util.IntoTensor] = None,
            noise_kernel: t.Optional[kernels.Kernel] = None,
            mean_func: t.Optional[MeanFunc] = None,
            solve_params: linalg.IntoSolveParams = None,
            det_params: linalg.IntoDetParams = None,
            preferred_dtype: t.Optional[tf.DType] = None,
            scope: t.Optional[t.Union[str, tf.VariableScope]] = None,
    ) -> 'Model':
        """Factory `classmethod` to make a new `Model` instance.

        Upon constructing, useful derived `Model` attributes are:
        * `f_out_mean`: Posterior mean.
        * `losses`: Components of posterior log likelihood.

        Args:
            X_in: Index points for input measurements.
            Y_in: Values for input measurements.
            kernel: Covariance kernel function.
            X_out: Optional. Index points for posterior computations. E.g.
                interpolation points.
            noise_kernel: Optional. Kernel representing measurement noise.
                Defaults to `WhiteNoise` with trainable noise level.
            mean_func: Optional. Mean function. Defaults to trainable constant
                mean.
            solve_params: Parameters for linear solves involved in posterior
                mean computation.
            det_params: Parameters for log determinants involved in posterior
                likelihood computation.
        """
        with tf.variable_scope(scope or 'GPScale'):
            with tf.name_scope('Initialization'):
                solve_params = linalg.SolveParams.make(solve_params)
                det_params = linalg.DetParams.make(det_params)

                noise_kernel = noise_kernel or kernels.WhiteNoise()
                mean_func = mean_func or ConstantMean()

                X_in, Y_in, X_out, dtype = cls._handle_input_tensors(
                    X_in=X_in,
                    Y_in=Y_in,
                    X_out=X_out,
                    preferred_dtype=preferred_dtype,
                )

            with tf.name_scope('Prediction'):
                K_in_in = _make_K_in_in(kernel, noise_kernel, X_in)
                mean_in = _make_mean(mean_func, X_in)

                Y_in_minus_mean = Y_in - mean_in
                sol_vec = linalg.solve(K_in_in, Y_in_minus_mean, solve_params)

                if X_out is not None:
                    K_out_in = _make_K_out_in(kernel, X_in, X_out)
                    mean_out = _make_mean(mean_func, X_out)

                    with tf.name_scope('K_out_in'):
                        f_out_mean = tf.squeeze(K_out_in * sol_vec) + mean_out
                else:
                    f_out_mean = None

            with tf.name_scope('Likelihood'):
                with tf.name_scope('DataFit'):
                    data_fit = -linalg.inv_quad_form(
                        A=K_in_in, x=Y_in_minus_mean, Ainv_x=sol_vec) / 2.
                with tf.name_scope('Complexity'):
                    complexity = -linalg.log_det(
                        K_in_in, params=det_params) / 2.
                with tf.name_scope('Norm'):
                    norm = -np.log(2 * np.pi) * tf.cast(
                        tf.shape(Y_in)[0], dtype) / 2.

                with tf.name_scope('Sum'):
                    log_likelihood = tf.add_n([data_fit, complexity, norm])

                losses = LossComponents(
                    log_likelihood=log_likelihood,
                    complexity=complexity,
                    data_fit=data_fit)

            return cls(
                X_in=X_in,
                X_out=X_out,
                Y_in=Y_in,
                f_out_mean=f_out_mean,
                losses=losses,
                kernel=kernel,
                noise_kernel=noise_kernel,
                mean_func=mean_func,
                solve_params=solve_params,
                det_params=det_params,
                dtype=dtype,
            )

    @classmethod
    def _handle_input_tensors(
            cls,
            *,
            X_in,
            Y_in,
            preferred_dtype,
            X_out,
    ) -> t.Tuple[T, T, T, tf.DType]:
        X_in = util.ensure_tensor(
            X_in,
            name='X_in',
            ensure_rank_is=2,
            preferred_dtype=preferred_dtype)
        dtype = X_in.dtype
        Y_in = util.ensure_tensor(
            Y_in, name='Y_in', ensure_rank_is=1, preferred_dtype=dtype)

        tensors = [X_in, Y_in]
        if X_out is not None:
            X_out = util.ensure_tensor(
                X_out, name='X_out', ensure_rank_is=2, preferred_dtype=dtype)
            tensors += [X_out]

        _check_dtypes_match(tensors)

        return X_in, Y_in, X_out, dtype


def _make_K_in_in(
        kernel: kernels.Kernel,
        noise_kernel: kernels.Kernel,
        X_in: T,
) -> linops.LinOp:
    with tf.variable_scope(_ReusedScopes.KERNEL, reuse=tf.AUTO_REUSE):
        return kernel.make_lin_op(X_in) + noise_kernel.make_lin_op(X_in)


def _make_K_out_in(
        kernel: kernels.Kernel,
        X_in: T,
        X_out: T,
) -> linops.LinOp:
    with tf.variable_scope(_ReusedScopes.KERNEL, reuse=tf.AUTO_REUSE):
        return kernel.make_lin_op(X_out, X_in)


def _make_mean(mean_func: MeanFunc, X: T) -> T:
    with tf.variable_scope(_ReusedScopes.MEAN, reuse=tf.AUTO_REUSE):
        return mean_func.mean_func(X)


def _check_dtypes_match(tensors):
    dtypes = {tensor.dtype for tensor in tensors}
    if len(dtypes) != 1:
        raise ValueError(f'Inputs had different dtypes: {tensors}')


class _ReusedScopes:
    KERNEL = 'Kernel'
    MEAN = 'Mean'
