import numpy as np
import pytest
import tensorflow as tf

from . import hyper


def test_log(tf_session):
    _test_projection(tf_session, hyper.LogPositive(init_value=1.))


def test_softplus(tf_session):
    _test_projection(tf_session, hyper.SoftPlusPositive(init_value=1.))


def _test_projection(tf_session, hyper_spec):
    np.testing.assert_allclose(
        tf_session.run(hyper_spec.from_latent(hyper_spec.to_latent(7.))), 7.)


@pytest.mark.parametrize('dtype', [tf.float32, tf.float64])
def test_into_tensor(tf_session, dtype):
    del tf_session

    assert not tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)

    var = tf.get_variable('blah', initializer=1., dtype=tf.float32)
    tensor = hyper.into_tensor(var, var_name='claw', dtype=dtype)
    assert isinstance(tensor, (tf.Tensor, tf.Variable))
    assert tensor.dtype.base_dtype == dtype
    assert len(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)) == 1


if __name__ == '__main__':
    pytest.main()
