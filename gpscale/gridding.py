"""Grid management routines."""

import abc
import typing as t

import tensorflow as tf

from . import interp
from . import util

T = tf.Tensor


class Grid(metaclass=abc.ABCMeta):
    """Base class for regular grids."""

    @abc.abstractmethod
    def make_grid_interp(self, points: T) -> interp.GridInterp:
        pass


@util.dataclass()
class Grid1D(Grid):
    """One-dimensional, regular grid."""
    min_val: util.IntoTensor
    max_val: util.IntoTensor
    point_count: util.IntoTensor

    def make_grid_points(self, dtype: tf.DType = tf.float32) -> 'Grid1D':
        min_val = util.ensure_tensor(self.min_val, ensure_dtype_is=dtype)
        max_val = util.ensure_tensor(self.max_val, ensure_dtype_is=dtype)
        point_count = util.ensure_tensor(
            self.point_count, ensure_dtype_is=tf.int32)
        return tf.lin_space(min_val, max_val, point_count)

    def make_grid_interp(self, points: T) -> interp.GridInterp:
        if not isinstance(points, T):
            raise TypeError
        points = util.ensure_rank(points, 1)
        if util.get_rank(points) != 1:
            raise ValueError(points)
        return interp.make_1d_linear_grid_interp(
            to_points=points, from_points=self.make_grid_points(points.dtype))

    def make_tau(self, dtype: tf.DType = tf.float32) -> T:
        points = self.make_grid_points(dtype)
        return points - points[0]


@util.dataclass()
class ProductGrid(Grid):
    """Cartesian product of `Grid1D`s."""
    axis_grids: t.Tuple[Grid, ...] = ()

    @property
    def point_counts(self) -> t.List[T]:
        return [grid.point_count for grid in self.axis_grids]

    @property
    def dim(self) -> int:
        return len(self.axis_grids)

    def make_grid_interp(self, points: util.IntoTensor) -> interp.GridInterp:
        points = util.ensure_tensor(
            points,
            name='points',
            ensure_rank_is=2,
        )
        grid_interps = [
            grid.make_grid_interp(points[:, axis])
            for axis, grid in enumerate(self.axis_grids)
        ]

        return interp.make_product_grid_interp(grid_interps)
