"""Routines for specifying hyperparameters."""

import abc
import typing as t

import numpy as np
import tensorflow as tf

from . import util

_T = tf.Tensor

IntoHyperSpec = t.Union['_HyperSpec', util.IntoTensor]


def unconstrained(init_value: util.IntoTensor, trainable: bool = True):
    """Make a specification for an unconstrained hyperparameter.

    Useful e.g. for means, which need not be positive.
    """
    return Unconstrained(init_value=init_value, trainable=trainable)


def positive(init_value: util.IntoTensor, trainable: bool = True):
    """Make a specification for a positive hyperparameter.

    Useful e.g. for variances, which must be positive.

    At the moment this just wraps `SoftPlusPositive`, but other options may be
    added.
    """
    return SoftPlusPositive(init_value=init_value, trainable=trainable)


def into_tensor(
        hyper_spec: IntoHyperSpec,
        var_name: str,
        dtype: tf.DType,
) -> _T:
    """Make a hyperparameter `Tensor`.

    If given a `_HyperSpec` subclass instance, generates and returns a new
    `tf.Variable`. Otherwise returns its input unmodified.
    """
    if isinstance(hyper_spec, _HyperSpec):
        return hyper_spec.make_tensor(var_name, dtype)
    return util.ensure_tensor(hyper_spec, ensure_dtype_is=dtype)


@util.dataclass(frozen=False)
class _HyperSpec(metaclass=abc.ABCMeta):
    """Base class for specifications of hyperparameters."""
    init_value: util.IntoTensor
    trainable: bool = True

    @abc.abstractmethod
    def make_tensor(self, var_name: str, dtype: tf.DType) -> _T:
        """Produce a `tf.Tensor` from the hyperparameter specification.

        Must be implemented by subclasses.
        """


class Unconstrained(_HyperSpec):
    """Specification for an unconstrained hyperparameter (e.g. means)."""

    def make_tensor(self, var_name: str, dtype: tf.DType) -> _T:
        return tf.get_variable(
            name=var_name,
            trainable=self.trainable,
            dtype=dtype,
            initializer=util.ensure_tensor(
                self.init_value, ensure_dtype_is=dtype),
        )


class _Projected(_HyperSpec):
    """Base class for hyperparameters constrained by a projection function."""

    def make_tensor(self, var_name: str, dtype: tf.DType) -> _T:
        with tf.name_scope(self.__class__.__name__):
            init_value = util.ensure_tensor(
                self.init_value, ensure_dtype_is=dtype)
            init_latent_value = self.to_latent(init_value)
            variable = tf.get_variable(
                name=self.var_name_prefix + var_name,
                trainable=self.trainable,
                dtype=dtype,
                initializer=init_latent_value,
            )
            return self.from_latent(variable)

    @property
    def var_name_prefix(self) -> str:
        return ''

    @abc.abstractmethod
    def to_latent(self, value: _T) -> _T:
        pass

    @abc.abstractmethod
    def from_latent(self, latent_value: _T) -> _T:
        pass


class LogPositive(_Projected):
    r"""Specification for a positive hyperparameter (e.g. variances).

    Based on the $\log$ <-> $\exp$ mapping.
    """

    def to_latent(self, value: _T) -> _T:
        return tf.log(value) / np.log(10.)

    def from_latent(self, latent_value: _T) -> _T:
        return tf.exp(np.log(10.) * latent_value)

    @property
    def var_name_prefix(self) -> str:
        return 'log10_'


class SoftPlusPositive(_Projected):
    r"""Specification for a positive hyperparameter (e.g. variances).

    Based on the softplus function, i.e. $\log(1 + \exp(x))$. Inspired by
    GPyTorch.

    Latent values << 0 can be interpreted as base-e logarithms of the "true"
    values, while latent values >> 0 can be interpreted as "true" values
    directly.
    """

    def to_latent(self, value: _T) -> _T:
        # The threshold of 20 ensures an error of ~< 1e-9.
        return tf.where(value > 20, value, tf.log(-1 + tf.exp(value)))

    def from_latent(self, latent_value: _T) -> _T:
        return tf.math.softplus(latent_value)

    @property
    def var_name_prefix(self) -> str:
        return 'softplus_'
