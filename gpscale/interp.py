"""Interpolation routines."""

import typing as t

import tensorflow as tf

from . import util

T = tf.Tensor


def make_1d_linear_grid_interp(from_points: T, to_points: T) -> 'GridInterp':
    """Make a sparse interpolation matrix from `from_points` to `to_points`.

    `from_points` must already be sorted.
    """
    to_points = util.ensure_tensor(to_points, name='points', ensure_rank_is=1)
    from_points = util.ensure_tensor(
        from_points, ensure_rank_is=1, preferred_dtype=to_points.dtype)

    num_from_points = tf.shape(from_points)[0]

    right_indices = util.searchsorted(
        from_points, to_points, dtype=num_from_points.dtype)
    left_indices = right_indices - 1

    # Handle extrapolation by reverting to a constant outside the region spanned
    # by `from_points`.
    extrap_right_mask = right_indices > num_from_points - 1
    extrap_left_mask = left_indices < 0
    right_indices = tf.minimum(right_indices, num_from_points - 1)
    left_indices = tf.maximum(left_indices, 0)

    right_weights = to_points - tf.gather(from_points, left_indices)
    left_weights = tf.gather(from_points, right_indices) - to_points

    def amend_weights(mask, weights):
        """Prevent negative weights (which occur upon extrapolation)."""
        return tf.cond(
            tf.reduce_any(mask),
            lambda: tf.where(mask, tf.ones_like(weights), weights),
            lambda: weights,
        )

    right_weights = amend_weights(extrap_left_mask, right_weights)
    left_weights = amend_weights(extrap_right_mask, left_weights)

    norm = left_weights + right_weights
    right_weights /= norm
    left_weights /= norm

    weights_by_row = tf.stack([left_weights, right_weights], axis=1)
    grid_inds_by_row = tf.stack([left_indices, right_indices], axis=1)

    return GridInterp.make(
        grid_inds_by_row=grid_inds_by_row[:, :, tf.newaxis],
        weights_by_row=weights_by_row,
        grid_point_counts=num_from_points[tf.newaxis],
    )


def make_product_grid_interp(
        grid_interps: t.Iterable['GridInterp']) -> 'GridInterp':
    """Make a `GridInterpolation` for a product grid.

    We are given specifications for interpolation matrices for each axis --
    e.g., in 3D,

        $$M^1_{ai}, M^2_{aj}, M^3_{ak}.$$

    Given these, we want to compute the Kronecker product tensor

        $$M_{aijk} = M^1_{ai} M^2_{aj} M^3_{ak},$$

    and then to flatten the $ijk$ indices to produce a matrix. This is
    trivial with dense matrices:

        ```python
        M = (
            M1[:, :, tf.newaxis, tf.newaxis]
            * M2[:, tf.newaxis, :, tf.newaxis]
            * M3[:, tf.newaxis, tf.newaxis, :]
        )
        M_flat = tf.reshape(M, (tf.shape(M)[0], -1))
        ```

    However, this kind of broadcasting/indexing isn't (yet?) possible with
    `tf.SparseTensor`s, so we have to roll it ourselves, which is what this
    function accomplishes.
    """
    grid_interps = tuple(grid_interps)

    grid_inds_by_row_by_axis, weights_by_row_by_axis = zip(
        # Remove the unnecessary "which grid" axis from `grid_inds_by_row`,
        # which is only actually needed for product grids.
        *(
            (
                grid_interp.grid_inds_by_row[:, :, 0],
                grid_interp.weights_by_row,
            ) for grid_interp in grid_interps))

    # Number of points to interpolate to.
    output_point_count = tf.cast(grid_interps[0].output_point_count, tf.int32)
    # Number of points used for each interpolation.
    grid_window_point_count = tf.shape(grid_inds_by_row_by_axis[0])[1]
    num_grids = len(grid_inds_by_row_by_axis)
    # Number of interpolation matrix entries for each point to interpolate
    # to.
    prod_grid_window_point_count = grid_window_point_count**num_grids

    def make_reshaper(axis):
        """Add length-1 axes to a tensor."""
        # Don't touch the output axis.
        reshaper = (slice(None), )
        reshaper += tuple(
            tf.newaxis if other_axis != axis else slice(None)
            for other_axis in range(num_grids))
        return reshaper

    def make_tile_spec(axis):
        """Perform broadcasting using `tf.tile`."""
        # Don't tile the output axis.
        tile_spec = (1, )
        tile_spec += tuple(
            grid_window_point_count if other_axis != axis else 1
            for other_axis in range(num_grids))
        return tile_spec

    tiled_grid_inds_by_row_by_axis = [
        tf.tile(grid_inds_by_row[make_reshaper(axis)], make_tile_spec(axis))
        for axis, grid_inds_by_row in enumerate(grid_inds_by_row_by_axis)
    ]
    stacked_shape = tf.stack(
        [output_point_count, prod_grid_window_point_count, num_grids], axis=0)
    prod_grid_inds_by_row = tf.reshape(
        tf.stack(tiled_grid_inds_by_row_by_axis, axis=num_grids + 1),
        stacked_shape)

    tiled_weights_by_row_by_axis = [
        tf.tile(weights_by_row[make_reshaper(axis)], make_tile_spec(axis))
        for axis, weights_by_row in enumerate(weights_by_row_by_axis)
    ]
    stacked_weights = tf.reshape(
        tf.stack(tiled_weights_by_row_by_axis, axis=num_grids + 1),
        stacked_shape)
    prod_weights_by_row = tf.reduce_prod(stacked_weights, axis=2)

    grid_point_counts = tf.concat(
        [grid_interp.grid_point_counts for grid_interp in grid_interps],
        axis=0)

    return GridInterp.make(
        grid_inds_by_row=prod_grid_inds_by_row,
        weights_by_row=prod_weights_by_row,
        grid_point_counts=grid_point_counts)


@util.dataclass(repr=False)
class GridInterp:
    """Class to produce `tf.SparseTensor`s for interpolation matrices.

    A `GridInterp` contains necessary precomputation weights to perform
    interpolation from values defined on a 1D or Cartesian product grid to
    output points within the grid's bounds. This can be accomplished with
    `interp_from_grid`. The adjoint is also available as `adjoint_to_grid`.
    """
    # Input attributes.
    grid_inds_by_row: T
    weights_by_row: T
    grid_point_counts: T

    # Derived attributes.
    output_point_count: T

    # 0th axis of `interp_tensor` is output point; the remainder axes correspond
    # to each grid axis.
    tensor_shape: T
    interp_tensor: T

    # 0th axis of `interp_matrix` is output point; 1st is all grid axes
    # flattened into a single axis.
    matrix_shape: T
    interp_matrix: T
    # 0th axis of `interp_matrix` is all grid axes flattened into a single axis;
    # 1st is output point.
    interp_matrix_transp: T

    @classmethod
    def make(
            cls,
            grid_inds_by_row: T,
            weights_by_row: T,
            grid_point_counts: T,
    ) -> 'GridInterp':
        """Factory `classmethod`.

        Args:
            grid_inds_by_row[i, j, k]:
                i: index of output point
                    len=self.output_point_count
                j: which point within interp window
                    len=interp_window_size**len(grid_point_counts)
                k: index of grid
                    len=len(grid_point_counts)
                value: index along grid
            weights_by_row[i, j]:
                i: index of output point
                    len=self.output_point_count
                j: which point within interp window
                    len=interp_window_size**len(grid_point_counts)
                value: weight of point specified by grid_inds_by_row[i, j, :]
            grid_point_counts[k]:
                k: index of grid
                value: number of points in grid k.
        """
        cls._check_inputs(grid_inds_by_row, weights_by_row, grid_point_counts)

        grid_inds_by_row = tf.cast(grid_inds_by_row, tf.int64)
        grid_point_counts = tf.cast(grid_point_counts, tf.int64)

        grid_inds_by_row_shape = tf.shape(grid_inds_by_row, out_type=tf.int64)
        output_point_count = grid_inds_by_row_shape[0]
        points_per_window = grid_inds_by_row_shape[1]
        grid_count = grid_inds_by_row_shape[2]
        num_entries = output_point_count * points_per_window

        tensor_shape = tf.concat(
            [
                [output_point_count],
                grid_point_counts,
            ], axis=0)
        matrix_shape = tf.stack(
            [
                output_point_count,
                tf.reduce_prod(grid_point_counts),
            ], axis=0)

        tile_spec = tf.stack([1, points_per_window, 1], axis=0)
        row_inds = tf.tile(
            tf.range(output_point_count,
                     dtype=tf.int64)[:, tf.newaxis, tf.newaxis], tile_spec)
        reshape_spec = tf.stack([num_entries, grid_count + 1], axis=0)
        inds = tf.reshape(
            tf.concat([row_inds, grid_inds_by_row], axis=2), reshape_spec)
        weights = tf.reshape(weights_by_row, num_entries[tf.newaxis])

        interp_tensor = tf.sparse_reorder(
            tf.SparseTensor(
                indices=inds,
                values=weights,
                dense_shape=tensor_shape,
            ))
        interp_matrix = tf.sparse_reshape(interp_tensor, matrix_shape)

        return cls(
            # Input attributes.
            grid_inds_by_row=grid_inds_by_row,
            weights_by_row=weights_by_row,
            grid_point_counts=grid_point_counts,
            # Derived attributes.
            output_point_count=output_point_count,
            tensor_shape=tensor_shape,
            matrix_shape=matrix_shape,
            interp_tensor=interp_tensor,
            interp_matrix=interp_matrix,
            interp_matrix_transp=tf.sparse_transpose(interp_matrix),
        )

    def interp_from_grid(self, grid_values: util.IntoTensor) -> T:
        """Interpolate grid values to the output points."""
        grid_values = util.ensure_tensor(
            grid_values, name='grid_values', preferred_dtype=self.dtype)
        from_grid_flat = tf.sparse_tensor_dense_matmul(
            self.interp_matrix,
            tf.reshape(grid_values, (-1, ))[:, tf.newaxis],
        )
        return tf.reshape(from_grid_flat, self.output_point_count[tf.newaxis])

    def adjoint_to_grid(self, values: util.IntoTensor) -> T:
        """Compute the interpolation adjoint from output points to the grid."""
        values = util.ensure_tensor(
            values, name='values', preferred_dtype=self.dtype)
        values = util.ensure_rank(values, 2)
        to_grid_flat = tf.sparse_tensor_dense_matmul(
            self.interp_matrix_transp, values)
        return tf.reshape(to_grid_flat, self.grid_point_counts)

    @property
    def dtype(self):
        return self.weights_by_row.dtype

    @staticmethod
    def _check_inputs(
            grid_inds_by_row: T,
            weights_by_row: T,
            grid_point_counts: T,
    ):
        if not isinstance(grid_inds_by_row, T):
            raise TypeError(grid_inds_by_row)
        if not isinstance(weights_by_row, T):
            raise TypeError(weights_by_row)
        if not isinstance(grid_point_counts, T):
            raise TypeError(grid_point_counts)

        ind_dims = len(grid_inds_by_row.get_shape())
        if ind_dims != 3:
            raise ValueError(ind_dims)
        weight_dims = len(weights_by_row.get_shape())
        if weight_dims != 2:
            raise ValueError(weight_dims)
        grid_point_counts_dims = len(grid_point_counts.get_shape())
        if grid_point_counts_dims != 1:
            raise ValueError(grid_point_counts_dims)
