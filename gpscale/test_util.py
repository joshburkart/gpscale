import tensorflow as tf

from . import util


@util.dataclass(frozen=False)
class SessionWrapper:
    session: tf.Session
    run_init: bool = True

    def run(self, *args, **kwargs):
        if self.run_init:
            self.session.run(tf.global_variables_initializer())
        return self.session.run(*args, **kwargs)
